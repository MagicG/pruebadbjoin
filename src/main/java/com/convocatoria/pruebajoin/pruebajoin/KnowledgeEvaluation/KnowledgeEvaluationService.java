package com.convocatoria.pruebajoin.pruebajoin.KnowledgeEvaluation;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class KnowledgeEvaluationService {

    @Autowired
    private KnowledgeEvaluationRepository knowledgeEvaluationRepository;


    public List<KnowledgeEvaluation> getAllKnowledgeEvaluation() {
        List<KnowledgeEvaluation> postulantes = new ArrayList<>();
        knowledgeEvaluationRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public KnowledgeEvaluation getKnowledgeEvaluation(long id) {
        return knowledgeEvaluationRepository.FindById(id)
                .orElse(null);

    }

    public long addKnowledgeEvaluation(KnowledgeEvaluation knowledgeEvaluation) {
        knowledgeEvaluationRepository.save(knowledgeEvaluation);
        return knowledgeEvaluationRepository.selectByIdknowledgeevaluation();
    }


    public void deleteKnowledgeEvaluation(long id) {
        knowledgeEvaluationRepository.DeleteByCi(id);
    }


    public List<KnowledgeEvaluation> listByKnow(long id){
        return knowledgeEvaluationRepository.findKnowledgeEvaluationsByKnowledge_Idknowledge(id);
    }
//
//    public List<Auxiliary> listadoPorCarrera(int career){
//
//        return auxiliaryRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<Auxiliary> QueryCiCodCareer(String ci, int career){
//
//        return auxiliaryRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }
}
