package com.convocatoria.pruebajoin.pruebajoin.KnowledgeEvaluation;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class KnowledgeEvaluationController {

    @Autowired
    private KnowledgeEvaluationService knowledgeEvaluationService;

    @RequestMapping("/knowledgeevaluation")
    public List<KnowledgeEvaluation> getAllKnowledgeEvaluation() {
        return knowledgeEvaluationService.getAllKnowledgeEvaluation();
    }

    @RequestMapping("/knowledgeevaluation/{id}")
    public KnowledgeEvaluation getKnowledgeEvaluation(@PathVariable long id) {
        return knowledgeEvaluationService.getKnowledgeEvaluation(id);
    }

    @PostMapping("/knowledgeevaluation")
    public long addKnowledgeEvaluation(@RequestBody KnowledgeEvaluation knowledgeEvaluation) {
        return knowledgeEvaluationService.addKnowledgeEvaluation(knowledgeEvaluation);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/knowledgeevaluation/{id}")
    public void deleteKnowledgeEvaluation(@PathVariable long id) {
        knowledgeEvaluationService.deleteKnowledgeEvaluation(id);
    }

    @RequestMapping("/knowledgeevaluation/listado/{ci}")
    public List<KnowledgeEvaluation> listByKnow(@PathVariable("ci") long id) {
        return knowledgeEvaluationService.listByKnow(id);
    }
//
//    @RequestMapping("/auxiliary/listar/{career}")
//    public List<Auxiliary> listadoPorCarrera(@PathVariable("career") int career) {
//        return auxiliaryService.listadoPorCarrera(career);
//    }
//
//    @RequestMapping("/auxiliary/listando/{ci}/{career}")
//    public List<Auxiliary> QueryCiCodCareer(@PathVariable("ci") String ci , @PathVariable("career") int career) {
//        return auxiliaryService.QueryCiCodCareer(ci,career);
//    }

//    @RequestMapping("/postulantes/listando/status")
//    public List<Boolean> getStatusFromPostulants() {
//        return postulantesService.getStatusFromPostulants();
//    }

}
