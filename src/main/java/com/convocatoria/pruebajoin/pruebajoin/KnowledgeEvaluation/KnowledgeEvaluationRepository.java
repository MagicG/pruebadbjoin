package com.convocatoria.pruebajoin.pruebajoin.KnowledgeEvaluation;

//import com.tis.convocatorias.postulant.service.Career.Career;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface KnowledgeEvaluationRepository extends JpaRepository<KnowledgeEvaluation, Long>{

    @Query("select item from KnowledgeEvaluation item where item.idknowledgeevaluation = :id_Postulant")
    Optional<KnowledgeEvaluation> FindById(@Param("id_Postulant") long id_Postulant);

//    @Modifying
//    @Transactional
//    @Query("update Auxiliary item set item.Status = :Status, item.person = :person, item.career = :career where item.id_Postulant = :id_Postulant")
//    void UpdateByCi(@Param("id_Postulant") long id_Postulant, @Param("Status") boolean Status, @Param("person") long person, @Param("career") int career);

    @Modifying
    @Transactional
    @Query("delete from KnowledgeEvaluation item where item.idknowledgeevaluation = :id_Postulant")
    void DeleteByCi(@Param("id_Postulant") long id_Postulant);


    @Query(value = "select MAX(idknowledgeevaluation) from knowledge_evaluation", nativeQuery = true)
    long selectByIdknowledgeevaluation();



    List<KnowledgeEvaluation> findKnowledgeEvaluationsByKnowledge_Idknowledge(long idknow);

}
