package com.convocatoria.pruebajoin.pruebajoin.KnowledgeEvaluation;

import com.convocatoria.pruebajoin.pruebajoin.AcademicUnit.AcademicUnit;
import com.convocatoria.pruebajoin.pruebajoin.Area.Area;
import com.convocatoria.pruebajoin.pruebajoin.Knowledge.Knowledge;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "KnowledgeEvaluation")
public class KnowledgeEvaluation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idknowledgeevaluation")
    private long idknowledgeevaluation;
    @Column
    private String description;
    @Column
    private int percentage;


//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idknowledge", referencedColumnName = "idknowledge")
//    @Column
    private Knowledge knowledge;

//
//    @ManyToOne
//    @JoinColumn(name="id_career")
//    private Career career;

    public KnowledgeEvaluation() {
    }

    public KnowledgeEvaluation(String description, int percentage, Knowledge knowledge) {
        this.description = description;
        this.percentage = percentage;
        this.knowledge = knowledge;
    }

    public long getIdknowledgeevaluation() {
        return idknowledgeevaluation;
    }

    public void setIdknowledgeevaluation(long idknowledgeevaluation) {
        this.idknowledgeevaluation = idknowledgeevaluation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public Knowledge getKnowledge() {
        return knowledge;
    }

    public void setKnowledge(Knowledge knowledge) {
        this.knowledge = knowledge;
    }
}
