package com.convocatoria.pruebajoin.pruebajoin.Management;//package com.tis.convocatorias.postulant.service.Career;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ManagementService {

    @Autowired
    private ManagementRepository managementRepository;

    public List<Management> getAllManagements() {
        //return postulants;
        List<Management> people = new ArrayList<>();
        managementRepository.findAll()
                .forEach(people::add);
        return people;
    }

    public Management getManagement(int ci) {
        //return postulants.stream().filter(t -> t.getId().equals(id)).findFirst().get();
//        String aux = id.toString();
//        return postulantRepository.getOne(id);

        return managementRepository.FindByCi(ci)
                .orElse(null);
    }


    public void addManagement(Management management) {

        managementRepository.save(management);
    }

//    public void updateManagement(int ci, Management management) {
//        managementRepository.UpdateByCi(ci, management.getPeriod());
//
//    }

    public void deleteManagement(int ci) {
        //postulants.removeIf(p -> p.getId().equals(id));
        managementRepository.DeleteByCi(ci);
    }
}
