package com.convocatoria.pruebajoin.pruebajoin.Management;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;

public interface ManagementRepository extends JpaRepository<Management, Long> {

    @Query("select item from Management item where item.idmanagement = :codCareer")
    Optional<Management> FindByCi(@Param("codCareer") int codCareer);

//    @Modifying
//    @Transactional
//    @Query("update Management item set item.name = :name where item.codCareer = :codCareer")
//    void UpdateByCi(@Param("codCareer") int codCareer, @Param("name") String name);

    @Modifying
    @Transactional
    @Query("delete from Management item where item.idmanagement = :codCareer")
    void DeleteByCi(@Param("codCareer") int id_career);
}
