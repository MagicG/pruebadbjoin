package com.convocatoria.pruebajoin.pruebajoin.Management;//package com.tis.convocatorias.postulant.service.Career;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "Management")
public class Management {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
////    @Column(name = "id", updatable = false, nullable = false
//    @Transient
    @Column(name="idmanagement")
    private long idmanagement;

    @Column
    private String period;



    public Management() {
    }

    public Management(String period) {
        this.period = period;
    }

    public long getIdmanagement() {
        return idmanagement;
    }

    public void setIdmanagement(long idmanagement) {
        this.idmanagement = idmanagement;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
}
