package com.convocatoria.pruebajoin.pruebajoin.Management;//package com.tis.convocatorias.postulant.service.Career;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class ManagementController {

    @Autowired
    private ManagementService managementService;

    @RequestMapping("/management")
    public List<Management> getAllManagement() {
        return managementService.getAllManagements();
    }

    @RequestMapping("/management/{id}")
    public Management getManagement(@PathVariable int id) {
        return managementService.getManagement(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/management")
    public long addManagement(@RequestBody Management management) {
        managementService.addManagement(management);
        return management.getIdmanagement();
    }

//    @RequestMapping(method = RequestMethod.PUT, value = "/management/{id}")
//    public void updateManagement(@RequestBody Management management, @PathVariable int id) {
//        managementService.updateManagement(id, management);
//    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/management/{id}")
    public void deleteManagement(@PathVariable int id) {
        managementService.deleteManagement(id);
    }




}
