package com.convocatoria.pruebajoin.pruebajoin.SubdetailMerit;

import com.convocatoria.pruebajoin.pruebajoin.AcademicUnit.AcademicUnit;
import com.convocatoria.pruebajoin.pruebajoin.Area.Area;
import com.convocatoria.pruebajoin.pruebajoin.DetailMerit.DetailMerit;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "SubdetailMerit")
public class SubdetailMerit {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idsubdetailmerit")
    private long idsubdetailmerit;
    @Column
    private String description;
    @Column
    private int percentage;


//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "iddetailmerit", referencedColumnName = "iddetailmerit")
//    @Column
    private DetailMerit detailMerit;


    public SubdetailMerit() {
    }

    public SubdetailMerit(String description, int percentage, DetailMerit detailMerit) {
        this.description = description;
        this.percentage = percentage;
        this.detailMerit = detailMerit;
    }

    public long getIdsubdetailmerit() {
        return idsubdetailmerit;
    }

    public void setIdsubdetailmerit(long idsubdetailmerit) {
        this.idsubdetailmerit = idsubdetailmerit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public DetailMerit getDetailMerit() {
        return detailMerit;
    }

    public void setDetailMerit(DetailMerit detailMerit) {
        this.detailMerit = detailMerit;
    }
}
