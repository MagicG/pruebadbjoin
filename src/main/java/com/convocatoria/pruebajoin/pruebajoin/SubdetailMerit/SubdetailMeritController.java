package com.convocatoria.pruebajoin.pruebajoin.SubdetailMerit;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class SubdetailMeritController {

    @Autowired
    private SubdetailMeritService subdetailMeritService;

    @RequestMapping("/subdetailmerit")
    public List<SubdetailMerit> getAllSubdetailMerit() {
        return subdetailMeritService.getAllSubdetailMerit();
    }

    @RequestMapping("/subdetailmerit/{id}")
    public SubdetailMerit getSubdetailMerit(@PathVariable long id) {
        return subdetailMeritService.getSubdetailMerit(id);
    }

    @PostMapping("/subdetailmerit")
    public long addSubdetailMerit(@RequestBody SubdetailMerit subdetailMerit) {
        return subdetailMeritService.addSubdetailMerit(subdetailMerit);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/subdetailmerit/{id}")
    public void deleteSubdetailMerit(@PathVariable long id) {
        subdetailMeritService.deleteSubdetailMerit(id);
    }

//    @RequestMapping("/auxiliary/listado/{ci}")
//    public List<Auxiliary> listadoJoin(@PathVariable("ci") String ci) {
//        return auxiliaryService.listadoJoin(ci);
//    }
//
//    @RequestMapping("/auxiliary/listar/{career}")
//    public List<Auxiliary> listadoPorCarrera(@PathVariable("career") int career) {
//        return auxiliaryService.listadoPorCarrera(career);
//    }
//
//    @RequestMapping("/auxiliary/listando/{ci}/{career}")
//    public List<Auxiliary> QueryCiCodCareer(@PathVariable("ci") String ci , @PathVariable("career") int career) {
//        return auxiliaryService.QueryCiCodCareer(ci,career);
//    }

//    @RequestMapping("/postulantes/listando/status")
//    public List<Boolean> getStatusFromPostulants() {
//        return postulantesService.getStatusFromPostulants();
//    }

}
