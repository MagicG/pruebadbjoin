package com.convocatoria.pruebajoin.pruebajoin.SubdetailMerit;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SubdetailMeritService {

    @Autowired
    private SubdetailMeritRepository subdetailMeritRepository;


    public List<SubdetailMerit> getAllSubdetailMerit() {
        List<SubdetailMerit> postulantes = new ArrayList<>();
        subdetailMeritRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public SubdetailMerit getSubdetailMerit(long id) {
        return subdetailMeritRepository.FindById(id)
                .orElse(null);

    }

    public long addSubdetailMerit(SubdetailMerit subdetailMerit) {
        subdetailMeritRepository.save(subdetailMerit);
        return subdetailMeritRepository.selectByIdsubdetailmerit();
    }


    public void deleteSubdetailMerit(long id) {
        subdetailMeritRepository.DeleteByCi(id);
    }


//    public List<Auxiliary> listadoJoin(String ci){
//        return auxiliaryRepository.findPostulantesByPerson_Ci(ci);
//    }
//
//    public List<Auxiliary> listadoPorCarrera(int career){
//
//        return auxiliaryRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<Auxiliary> QueryCiCodCareer(String ci, int career){
//
//        return auxiliaryRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }
}
