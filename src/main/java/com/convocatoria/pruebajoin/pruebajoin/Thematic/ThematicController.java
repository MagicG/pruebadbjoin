package com.convocatoria.pruebajoin.pruebajoin.Thematic;//package com.tis.convocatorias.postulant.service.Career;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class ThematicController {

    @Autowired
    private ThematicService thematicService;

    @RequestMapping("/thematic")
    public List<Thematic> getAllThematic() {
        return thematicService.getAllThematic();
    }

    @RequestMapping("/thematic/{id}")
    public Thematic getThematic(@PathVariable int id) {
        return thematicService.getThematic(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/thematic")
    public long addThematic(@RequestBody Thematic thematic) {
        thematicService.addThematic(thematic);
        return thematic.getIdthematic();
    }

//    @RequestMapping(method = RequestMethod.PUT, value = "/academicunit/{id}")
//    public void updateAcademicUnit(@RequestBody AcademicUnit academicUnit, @PathVariable int id) {
//        academicUnitService.updateAcademicUnit(id, academicUnit);
//    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/thematic/{id}")
    public void deleteThematic(@PathVariable int id) {
        thematicService.deleteThematic(id);
    }
}
