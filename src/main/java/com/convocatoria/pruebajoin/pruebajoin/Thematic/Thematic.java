package com.convocatoria.pruebajoin.pruebajoin.Thematic;//package com.tis.convocatorias.postulant.service.Career;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "Thematic")
public class Thematic {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
////    @Column(name = "id", updatable = false, nullable = false
//    @Transient
    @Column(name="idthematic")
    private long idthematic;

    @Column
    private String name;



    public Thematic() {
    }

    public Thematic(String name) {
        this.name = name;
    }

    public long getIdthematic() {
        return idthematic;
    }

    public void setIdthematic(long idthematic) {
        this.idthematic = idthematic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
