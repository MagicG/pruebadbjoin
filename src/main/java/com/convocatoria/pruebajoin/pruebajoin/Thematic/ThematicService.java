package com.convocatoria.pruebajoin.pruebajoin.Thematic;//package com.tis.convocatorias.postulant.service.Career;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ThematicService {

    @Autowired
    private ThematicRepository thematicRepository;

    public List<Thematic> getAllThematic() {
        List<Thematic> people = new ArrayList<>();
        thematicRepository.findAll()
                .forEach(people::add);
        return people;
    }

    public Thematic getThematic(int ci) {
        return thematicRepository.FindByCi(ci)
                .orElse(null);
    }


    public void addThematic(Thematic thematic) {

        thematicRepository.save(thematic);
    }

//    public void updateAcademicUnit(int ci, AcademicUnit academicUnit) {
//        academicUnitRepository.UpdateByCi(ci, academicUnit.getName());
//
//    }

    public void deleteThematic(int ci) {
        thematicRepository.DeleteByCi(ci);
    }
}
