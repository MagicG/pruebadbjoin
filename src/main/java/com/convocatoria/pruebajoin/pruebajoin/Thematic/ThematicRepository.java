package com.convocatoria.pruebajoin.pruebajoin.Thematic;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;

public interface ThematicRepository extends JpaRepository<Thematic, Long> {

    @Query("select item from Thematic item where item.idthematic = :codCareer")
    Optional<Thematic> FindByCi(@Param("codCareer") int codCareer);

//    @Modifying
//    @Transactional
//    @Query("update AcademicUnit item set item.name = :name where item.id_aca_unit = :codCareer")
//    void UpdateByCi(@Param("codCareer") int codCareer, @Param("name") String name);

    @Modifying
    @Transactional
    @Query("delete from Thematic item where item.idthematic = :codCareer")
    void DeleteByCi(@Param("codCareer") int id_career);
}
