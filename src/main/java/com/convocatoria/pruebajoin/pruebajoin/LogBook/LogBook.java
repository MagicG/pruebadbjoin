package com.convocatoria.pruebajoin.pruebajoin.LogBook;

import com.convocatoria.pruebajoin.pruebajoin.Announcement.Announcement;
import com.convocatoria.pruebajoin.pruebajoin.Auxiliary.Auxiliary;
import com.convocatoria.pruebajoin.pruebajoin.Postulantes.Postulantes;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "LogBook")
public class LogBook {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idlogbook")
    private long idlogbook;
    @Column
//    @Temporal(TemporalType.TIMESTAMP)
//    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date deliveryHour;
    @Column
//    @Temporal(TemporalType.TIMESTAMP)
//    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date deliveryDate;
    @Column
    private int document_quantity;

//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idpostulant", referencedColumnName = "idpostulant")
//    @Column
    private Postulantes postulantes;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idauxiliary", referencedColumnName = "idauxiliary")
//    @Column
    private Auxiliary auxiliary;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idannouncement", referencedColumnName = "idannouncement")
//    @Column
    private Announcement announcement;

//
//    @ManyToOne
//    @JoinColumn(name="id_career")
//    private Career career;

    public LogBook() {
    }

    public LogBook(Date deliveryHour, Date deliveryDate, int document_quantity, Postulantes postulantes, Auxiliary auxiliary, Announcement announcement) {
        this.deliveryHour = deliveryHour;
        this.deliveryDate = deliveryDate;
        this.document_quantity = document_quantity;
        this.postulantes = postulantes;
        this.auxiliary = auxiliary;
        this.announcement = announcement;
    }

    public long getIdlogbook() {
        return idlogbook;
    }

    public void setIdlogbook(long idlogbook) {
        this.idlogbook = idlogbook;
    }

    public Date getDeliveryHour() {
        return deliveryHour;
    }

    public void setDeliveryHour(Date deliveryHour) {
        this.deliveryHour = deliveryHour;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public int getDocument_quantity() {
        return document_quantity;
    }

    public void setDocument_quantity(int document_quantity) {
        this.document_quantity = document_quantity;
    }

    public Postulantes getPostulantes() {
        return postulantes;
    }

    public void setPostulantes(Postulantes postulantes) {
        this.postulantes = postulantes;
    }

    public Auxiliary getAuxiliary() {
        return auxiliary;
    }

    public void setAuxiliary(Auxiliary auxiliary) {
        this.auxiliary = auxiliary;
    }

    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }
}
