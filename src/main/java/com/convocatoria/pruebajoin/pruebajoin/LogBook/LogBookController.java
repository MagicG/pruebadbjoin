package com.convocatoria.pruebajoin.pruebajoin.LogBook;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class LogBookController {

    @Autowired
    private LogBookService logBookService;

    @RequestMapping("/logbook")
    public List<LogBook> getAllLogBook() {
        return logBookService.getAllLogBook();
    }

    @RequestMapping("/logbook/{id}")
    public LogBook getLogBook(@PathVariable long id) {
        return logBookService.getLogBook(id);
    }

    @PostMapping("/logbook")
    public long addLogBook(@RequestBody LogBook logBook) {
        return logBookService.addLogBook(logBook);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/logbook/{id}")
    public void deleteLogBook(@PathVariable long id) {
        logBookService.deleteLogBook(id);
    }

    @RequestMapping("/logbook/listado/{ci}")
    public List<LogBook> listadoAnnouncement(@PathVariable("ci") long ci) {
        return logBookService.listadoAnnouncement(ci);
    }

    @RequestMapping("/logbook/listado/habilitados/{ci}")
    public List<LogBook> listadoAnnouncementByStatusTrue(@PathVariable("ci") long ci) {
        return logBookService.listadoAnnouncementByStatusTrue(ci);
    }

    @RequestMapping("/logbook/listado/inhabilitados/{ci}")
    public List<LogBook> listadoAnnouncementByStatusFalse(@PathVariable("ci") long ci) {
        return logBookService.listadoAnnouncementByStatusFalse(ci);
    }

//
//    @RequestMapping("/logbook/listar/{career}")
//    public List<LogBook> listadoPorCarrera(@PathVariable("career") int career) {
//        return logBookService.listadoPorCarrera(career);
//    }
//
//    @RequestMapping("/logbook/listando/{ci}/{career}")
//    public List<LogBook> QueryCiCodCareer(@PathVariable("ci") String ci , @PathVariable("career") int career) {
//        return logBookService.QueryCiCodCareer(ci,career);
//    }

//    @RequestMapping("/postulantes/listando/status")
//    public List<Boolean> getStatusFromPostulants() {
//        return postulantesService.getStatusFromPostulants();
//    }

}
