package com.convocatoria.pruebajoin.pruebajoin.LogBook;

//import com.tis.convocatorias.postulant.service.Career.Career;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface LogBookRepository extends JpaRepository<LogBook, Long>{

    @Query("select item from LogBook item where item.idlogbook = :id_Postulant")
    Optional<LogBook> FindById(@Param("id_Postulant") long id_Postulant);

//    @Modifying
//    @Transactional
//    @Query("update LogBook item set item.Status = :Status, item.person = :person, item.career = :career where item.id_Postulant = :id_Postulant")
//    void UpdateByCi(@Param("id_Postulant") long id_Postulant, @Param("Status") boolean Status, @Param("person") long person, @Param("career") int career);

    @Modifying
    @Transactional
    @Query("delete from LogBook item where item.idlogbook = :id_Postulant")
    void DeleteByCi(@Param("id_Postulant") long id_Postulant);


    @Query(value = "select MAX(idlogbook) from log_book", nativeQuery = true)
    long selectByIdlogbook();

//    SELECT postulant.*, person.name, career.name FROM `postulant` INNER JOIN person, career where person.ci = postulant.person AND postulant.career = career.cod_career
//    @Query("SELECT status FROM Postulantes ")
    List<LogBook> findLogBookByAnnouncement_Idannouncement(long id_announ);

    List<LogBook> findLogBookByAnnouncement_IdannouncementAndPostulantes_StatusIsTrue(long id_announ);

    List<LogBook> findLogBookByAnnouncement_IdannouncementAndPostulantes_StatusIsFalse(long id_announ);
//    List<LogBook> findPostulantesByPerson_Ci(String ci);
////    List<Postulantes> findPostulantesByPerson_Ci(String ci);
//    List<LogBook> findByCareer_CodCareerOrderByPerson(int career);
//
//    List<LogBook> queryByPerson_CiAndCareer_CodCareer(String ci, int codCareer);
//
//    @Query("SELECT status FROM LogBook ")
//    List<LogBook> find();
}
