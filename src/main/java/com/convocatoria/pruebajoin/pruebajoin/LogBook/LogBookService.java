package com.convocatoria.pruebajoin.pruebajoin.LogBook;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogBookService {



    @Autowired
    private LogBookRepository logBookRepository;


    public List<LogBook> getAllLogBook() {
        List<LogBook> postulantes = new ArrayList<>();
        logBookRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public LogBook getLogBook(long id) {
        return logBookRepository.FindById(id)
                .orElse(null);

    }

    public long addLogBook(LogBook logBook) {
        logBookRepository.save(logBook);
        return logBookRepository.selectByIdlogbook();
    }

    public void deleteLogBook(long id) {
        logBookRepository.DeleteByCi(id);
    }


    public List<LogBook> listadoAnnouncement(long id){

        return logBookRepository.findLogBookByAnnouncement_Idannouncement(id);
    }

    public List<LogBook> listadoAnnouncementByStatusTrue(long id){

        return logBookRepository.findLogBookByAnnouncement_IdannouncementAndPostulantes_StatusIsTrue(id);
    }

    public List<LogBook> listadoAnnouncementByStatusFalse(long id){

        return logBookRepository.findLogBookByAnnouncement_IdannouncementAndPostulantes_StatusIsFalse(id);
    }
//
//    public List<LogBook> listadoPorCarrera(int career){
//
//        return logBookRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<LogBook> QueryCiCodCareer(String ci, int career){
//
//        return logBookRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }

}
