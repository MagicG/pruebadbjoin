package com.convocatoria.pruebajoin.pruebajoin.Postulantes;

import com.convocatoria.pruebajoin.pruebajoin.Career.Career;
import com.convocatoria.pruebajoin.pruebajoin.Person.Person;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "Postulant")
public class Postulantes {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idpostulant")
    private long idpostulant;
    @Column
    private boolean status;

//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idperson", referencedColumnName = "idperson")
//    @Column
    private Person person;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idcareer", referencedColumnName = "idcareer")
//    @Column
    private Career career;

//
//    @ManyToOne
//    @JoinColumn(name="id_career")
//    private Career career;

    public Postulantes() {
    }

    public Postulantes(boolean status, Person person, Career career) {
        this.status = status;
        this.person = person;
        this.career = career;
    }

    public long getIdpostulant() {
        return idpostulant;
    }

    public void setIdpostulant(long idpostulant) {
        this.idpostulant = idpostulant;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Career getCareer() {
        return career;
    }

    public void setCareer(Career career) {
        this.career = career;
    }

    //    public Career getCareer() {
//        return career;
//    }
//
//    public void setCareer(Career career) {
//        this.career = career;
//    }
}
