package com.convocatoria.pruebajoin.pruebajoin.Postulantes;


import com.convocatoria.pruebajoin.pruebajoin.Person.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class PostulantesController {

    @Autowired
    private PostulantesService postulantesService;

    @RequestMapping("/postulantes")
    public List<Postulantes> getAllPostulantes() {
        return postulantesService.getAllPostulantes();
    }

    @RequestMapping("/postulantes/{id}")
    public Postulantes getPostulantes(@PathVariable long id) {
        return postulantesService.getPostulantes(id);
    }

    @PostMapping("/postulantes")
    public long addPostulantes(@RequestBody Postulantes postulantes) {
        return postulantesService.addPostulantes(postulantes);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/postulantes/{id}")
    public void deletePostulantes(@PathVariable long id) {
        postulantesService.deletePostulantes(id);
    }

    @RequestMapping("/postulantes/listado/{ci}")
    public List<Postulantes> listadoJoin(@PathVariable("ci") String ci) {
        return postulantesService.listadoJoin(ci);
    }

    @RequestMapping("/postulantes/listar/{career}")
    public List<Postulantes> listadoPorCarrera(@PathVariable("career") int career) {
        return postulantesService.listadoPorCarrera(career);
    }

    @RequestMapping("/postulantes/listando/{ci}/{career}")
    public List<Postulantes> QueryCiCodCareer(@PathVariable("ci") String ci ,@PathVariable("career") int career) {
        return postulantesService.QueryCiCodCareer(ci,career);
    }

//    @RequestMapping("/postulantes/listando/status")
//    public List<Boolean> getStatusFromPostulants() {
//        return postulantesService.getStatusFromPostulants();
//    }

}
