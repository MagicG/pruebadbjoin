package com.convocatoria.pruebajoin.pruebajoin.Postulantes;

//import com.tis.convocatorias.postulant.service.Career.Career;

import com.convocatoria.pruebajoin.pruebajoin.Career.Career;
import com.convocatoria.pruebajoin.pruebajoin.Person.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface PostulantRepository extends JpaRepository<Postulantes, Long>{

    @Query("select item from Postulantes item where item.idpostulant = :id_Postulant")
    Optional<Postulantes> FindById(@Param("id_Postulant") long id_Postulant);

    @Modifying
    @Transactional
    @Query("update Postulantes item set item.status = :Status, item.person = :person, item.career = :career where item.idpostulant = :id_Postulant")
    void UpdateByCi(@Param("id_Postulant") long id_Postulant, @Param("Status") boolean Status, @Param("person") long person, @Param("career") int career);

    @Modifying
    @Transactional
    @Query("delete from Postulantes item where item.idpostulant = :id_Postulant")
    void DeleteByCi(@Param("id_Postulant") long id_Postulant);

//    SELECT postulant.*, person.name, career.name FROM `postulant` INNER JOIN person, career where person.ci = postulant.person AND postulant.career = career.cod_career
//    @Query("SELECT status FROM Postulantes ")
//    List<Boolean> findPersonByStatus();
    List<Postulantes> findPostulantesByPerson_Ci(String ci);
//    List<Postulantes> findPostulantesByPerson_Ci(String ci);
    List<Postulantes> findByCareer_CodCareerOrderByPerson(int career);

    List<Postulantes> queryByPerson_CiAndCareer_CodCareer(String ci, int codCareer);

    @Query(value = "select MAX(idpostulant) from postulant", nativeQuery = true)
    long selectByIdpostulant();

//    @Query("SELECT status FROM Postulantes ")
//    List<Postulantes> find();
}
