package com.convocatoria.pruebajoin.pruebajoin.Postulantes;


import com.convocatoria.pruebajoin.pruebajoin.Person.Person;
import com.convocatoria.pruebajoin.pruebajoin.Person.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostulantesService {


//    private Management man1 = new Management("1","2020");
//    private Academic_unit aunit1 = new Academic_unit("1", "Departamento de Informatica Sistemas");
//    private Area a1 = new Area("1", "Laboratorio");
//    private Announcement ann1 = new Announcement("1", "CONVOCATORIA A CONCURSO DE MÉRITOS Y PRUEBAS DE CONOCIMIENTOS PARA OPTAR A AUXILIATURAS EN LABORATORIO DE COMPUTACIÓN, DE MANTENIMIENTO Y DESARROLLO",
//                                                "El Departamento de Informática y Sistemas junto a las Carreras de Ing. Informática e Ing. de Sistemas, de la Facultad de Ciencias y Tecnología, convoca al concurso de méritos y examen de competencia para la provisión de Auxiliares del Departamento, tomando como base los requerimientos que se tienen programados para la gestión 2020.",
//                                                "INFSIS-LAB-2020", "07/02/2020 11:00", "Los Honorables Consejos de Carrera de Informática y Sistemas designarán respectivamente; para la calificación de méritos 1 docente y 1 delegado estudiante, de la misma manera para la comisión de conocimientos cada consejo designará 1 docente y un estudiante veedor por cada temática.",
//                                                "Una vez concluido el proceso, la jefatura  decidirá qué auxiliares serán seleccionados para cada ítem, considerando los resultados finales y  las necesidades propias de cada laboratorio. Los nombramientos de auxiliar universitario titular recaerán sobre aquellos postulantes que hubieran aprobado y obtenido mayor calificación. Caso contrario se procederá con el nombramiento de aquel que tenga la calificación mayor como auxiliar invitado. Cabe resaltar que un auxiliar invitado solo tendrá nombramiento por los periodos académicos del semestre I y II de 2020.",
//                                                aunit1, man1, a1);
//    private Auxiliary aux1 = new Auxiliary("5", "Administrador de Laboratorio de Cómputo", "LCO-ADM", "80 Hrs/mes", aunit1, a1);
//    private Person per1 = new Person("3", "Rosa", "Martinez", "Av. Oquendo 3514", 73249814, "rosita@gmail.com");
//
//    private List<Postulantes> postulantess = new ArrayList<>(Arrays.asList(
//            new Postulantes( "1", ann1, aux1, per1, true, 0, 0, "Informática" ),
//            new Postulantes("2", ann1, aux1, per1, false, 0, 0, "Sistemas" ),
//            new Postulantes("3", ann1, aux1, per1, false, 0, 0, "Electrónica" ),
//            new Postulantes("4", ann1, aux1, per1, true, 0, 0, "Informática" ),
//            new Postulantes("5", ann1, aux1, per1, true, 0, 0, "Informática" )
//    ));

    @Autowired
    private PostulantRepository postulantRepository;


    public List<Postulantes> getAllPostulantes() {
        List<Postulantes> postulantes = new ArrayList<>();
        postulantRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public Postulantes getPostulantes(long id) {
        return postulantRepository.FindById(id)
                .orElse(null);

    }

    public long addPostulantes(Postulantes postulantes) {
        postulantRepository.save(postulantes);
        return postulantRepository.selectByIdpostulant();
    }

//    public void updatePostulantes(long id, Postulantes postulantes) {
//        List<Postulantes> postulant = new ArrayList<>();
//        postulantRepository.UpdateByCi(id, postulantes.isStatus(), postulantes.getPerson(), postulantes.getCareer());
//
//    }

    public void deletePostulantes(long id) {
        postulantRepository.DeleteByCi(id);
    }


    public List<Postulantes> listadoJoin(String ci){

        return postulantRepository.findPostulantesByPerson_Ci(ci);
    }

    public List<Postulantes> listadoPorCarrera(int career){

        return postulantRepository.findByCareer_CodCareerOrderByPerson(career);
    }
    public List<Postulantes> QueryCiCodCareer(String ci, int career){

        return postulantRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
    }
//    public List<Boolean> getStatusFromPostulants(){
//
//        return postulantRepository.getStatusPostulantes();
//    }

}
