package com.convocatoria.pruebajoin.pruebajoin.SubsubdetailMerit;

import com.convocatoria.pruebajoin.pruebajoin.AcademicUnit.AcademicUnit;
import com.convocatoria.pruebajoin.pruebajoin.Area.Area;
import com.convocatoria.pruebajoin.pruebajoin.SubdetailMerit.SubdetailMerit;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "SubsubdetailMerit")
public class SubsubdetailMerit {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idsubsubdetailmerit")
    private long idsubsubdetailmerit;
    @Column
    private String description;
    @Column
    private int percentage;


//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idsubdetailmerit", referencedColumnName = "idsubdetailmerit")
//    @Column
    private SubdetailMerit subdetailMerit;


//
//    @ManyToOne
//    @JoinColumn(name="id_career")
//    private Career career;

    public SubsubdetailMerit() {
    }

    public SubsubdetailMerit(String description, int percentage, SubdetailMerit subdetailMerit) {
        this.description = description;
        this.percentage = percentage;
        this.subdetailMerit = subdetailMerit;
    }

    public long getIdsubsubdetailmerit() {
        return idsubsubdetailmerit;
    }

    public void setIdsubsubdetailmerit(long idsubsubdetailmerit) {
        this.idsubsubdetailmerit = idsubsubdetailmerit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public SubdetailMerit getSubdetailMerit() {
        return subdetailMerit;
    }

    public void setSubdetailMerit(SubdetailMerit subdetailMerit) {
        this.subdetailMerit = subdetailMerit;
    }
}
