package com.convocatoria.pruebajoin.pruebajoin.SubsubdetailMerit;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class SubsubdetailMeritController {

    @Autowired
    private SubsubdetailMeritService subsubdetailMeritService;

    @RequestMapping("/subsubdetailmerit")
    public List<SubsubdetailMerit> getAllSubsubdetailMerit() {
        return subsubdetailMeritService.getAllSubsubdetailMerit();
    }

    @RequestMapping("/subsubdetailmerit/{id}")
    public SubsubdetailMerit getSubsubdetailMerit(@PathVariable long id) {
        return subsubdetailMeritService.getSubsubdetailMerit(id);
    }

    @PostMapping("/subsubdetailmerit")
    public long addSubsubdetailMerit(@RequestBody SubsubdetailMerit subsubdetailMerit) {
       return subsubdetailMeritService.addSubsubdetailMerit(subsubdetailMerit);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/subsubdetailmerit/{id}")
    public void deleteSubsubdetailMerit(@PathVariable long id) {
        subsubdetailMeritService.deleteSubsubdetailMerit(id);
    }

//    @RequestMapping("/auxiliary/listado/{ci}")
//    public List<Auxiliary> listadoJoin(@PathVariable("ci") String ci) {
//        return auxiliaryService.listadoJoin(ci);
//    }
//
//    @RequestMapping("/auxiliary/listar/{career}")
//    public List<Auxiliary> listadoPorCarrera(@PathVariable("career") int career) {
//        return auxiliaryService.listadoPorCarrera(career);
//    }
//
//    @RequestMapping("/auxiliary/listando/{ci}/{career}")
//    public List<Auxiliary> QueryCiCodCareer(@PathVariable("ci") String ci , @PathVariable("career") int career) {
//        return auxiliaryService.QueryCiCodCareer(ci,career);
//    }

//    @RequestMapping("/postulantes/listando/status")
//    public List<Boolean> getStatusFromPostulants() {
//        return postulantesService.getStatusFromPostulants();
//    }

}
