package com.convocatoria.pruebajoin.pruebajoin.SubsubdetailMerit;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SubsubdetailMeritService {

    @Autowired
    private SubsubdetailMeritRepository subsubdetailMeritRepository;


    public List<SubsubdetailMerit> getAllSubsubdetailMerit() {
        List<SubsubdetailMerit> postulantes = new ArrayList<>();
        subsubdetailMeritRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public SubsubdetailMerit getSubsubdetailMerit(long id) {
        return subsubdetailMeritRepository.FindById(id)
                .orElse(null);

    }

    public long addSubsubdetailMerit(SubsubdetailMerit subsubdetailMerit) {
        subsubdetailMeritRepository.save(subsubdetailMerit);
        return subsubdetailMeritRepository.selectByIdsubsubdetailmerit();
    }


    public void deleteSubsubdetailMerit(long id) {
        subsubdetailMeritRepository.DeleteByCi(id);
    }


//    public List<Auxiliary> listadoJoin(String ci){
//        return auxiliaryRepository.findPostulantesByPerson_Ci(ci);
//    }
//
//    public List<Auxiliary> listadoPorCarrera(int career){
//
//        return auxiliaryRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<Auxiliary> QueryCiCodCareer(String ci, int career){
//
//        return auxiliaryRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }
}
