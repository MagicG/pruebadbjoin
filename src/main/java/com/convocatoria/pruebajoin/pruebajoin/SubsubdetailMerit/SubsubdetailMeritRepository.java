package com.convocatoria.pruebajoin.pruebajoin.SubsubdetailMerit;

//import com.tis.convocatorias.postulant.service.Career.Career;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;

public interface SubsubdetailMeritRepository extends JpaRepository<SubsubdetailMerit, Long>{

    @Query("select item from SubsubdetailMerit item where item.idsubsubdetailmerit = :id_Postulant")
    Optional<SubsubdetailMerit> FindById(@Param("id_Postulant") long id_Postulant);

//    @Modifying
//    @Transactional
//    @Query("update Auxiliary item set item.Status = :Status, item.person = :person, item.career = :career where item.id_Postulant = :id_Postulant")
//    void UpdateByCi(@Param("id_Postulant") long id_Postulant, @Param("Status") boolean Status, @Param("person") long person, @Param("career") int career);

    @Modifying
    @Transactional
    @Query("delete from SubsubdetailMerit item where item.idsubsubdetailmerit = :id_Postulant")
    void DeleteByCi(@Param("id_Postulant") long id_Postulant);

    @Query(value = "select MAX(idsubsubdetailmerit) from subsubdetail_merit", nativeQuery = true)
    long selectByIdsubsubdetailmerit();


//    List<Auxiliary> findPostulantesByPerson_Ci(String ci);
////    List<Postulantes> findPostulantesByPerson_Ci(String ci);
//    List<Auxiliary> findByCareer_CodCareerOrderByPerson(int career);
//
//    List<Auxiliary> queryByPerson_CiAndCareer_CodCareer(String ci, int codCareer);
//
//    @Query("SELECT status FROM Auxiliary ")
//    List<Auxiliary> find();
}
