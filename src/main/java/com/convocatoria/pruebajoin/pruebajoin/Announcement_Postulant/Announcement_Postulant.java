package com.convocatoria.pruebajoin.pruebajoin.Announcement_Postulant;

import com.convocatoria.pruebajoin.pruebajoin.Announcement.Announcement;
import com.convocatoria.pruebajoin.pruebajoin.Postulantes.Postulantes;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "Announcement_Postulant")
public class Announcement_Postulant {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idannpost")
    private long idannpost;

//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idpostulant", referencedColumnName = "idpostulant")
//    @Column
    private Postulantes postulantes;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idannouncement", referencedColumnName = "idannouncement")
//    @Column
    private Announcement announcement;

//
//    @ManyToOne
//    @JoinColumn(name="id_career")
//    private Career career;

    public Announcement_Postulant() {
    }

    public Announcement_Postulant(Postulantes postulantes, Announcement announcement) {
        this.postulantes = postulantes;
        this.announcement = announcement;
    }


    public long getIdannpost() {
        return idannpost;
    }

    public void setIdannpost(long idannpost) {
        this.idannpost = idannpost;
    }

    public Postulantes getPostulantes() {
        return postulantes;
    }

    public void setPostulantes(Postulantes postulantes) {
        this.postulantes = postulantes;
    }

    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }
}
