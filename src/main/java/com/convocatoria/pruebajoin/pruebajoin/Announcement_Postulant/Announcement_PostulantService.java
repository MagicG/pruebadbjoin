package com.convocatoria.pruebajoin.pruebajoin.Announcement_Postulant;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class Announcement_PostulantService {


    @Autowired
    private Announcement_PostulantRepository announcementPostulantRepository;


    public List<Announcement_Postulant> getAllAnnounPostulant() {
        List<Announcement_Postulant> postulantes = new ArrayList<>();
        announcementPostulantRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public Announcement_Postulant getAnnounPostulant(long id) {
        return announcementPostulantRepository.FindById(id)
                .orElse(null);

    }

    public long addAnnounPostulant(Announcement_Postulant announcementPostulant) {
        announcementPostulantRepository.save(announcementPostulant);
        return announcementPostulantRepository.selectByIdannounpost();
    }

//    public void updatePostulantes(long id, Postulantes postulantes) {
//        List<Postulantes> postulant = new ArrayList<>();
//        postulantRepository.UpdateByCi(id, postulantes.isStatus(), postulantes.getPerson(), postulantes.getCareer());
//
//    }

    public void deleteAnnounPostulant(long id) {
        announcementPostulantRepository.DeleteByCi(id);
    }


//    public List<Announcement_Postulant> listadoJoin(String ci){
//
//        return announcementPostulantRepository.findPostulantesByPerson_Ci(ci);
//    }
//
//    public List<Announcement_Postulant> listadoPorCarrera(int career){
//
//        return announcementPostulantRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<Announcement_Postulant> QueryCiCodCareer(String ci, int career){
//
//        return announcementPostulantRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }
//    public List<Boolean> getStatusFromPostulants(){
//
//        return postulantRepository.getStatusPostulantes();
//    }

}
