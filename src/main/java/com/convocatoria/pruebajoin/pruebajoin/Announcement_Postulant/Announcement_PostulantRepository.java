package com.convocatoria.pruebajoin.pruebajoin.Announcement_Postulant;

//import com.tis.convocatorias.postulant.service.Career.Career;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface Announcement_PostulantRepository extends JpaRepository<Announcement_Postulant, Long>{

    @Query("select item from Announcement_Postulant item where item.idannpost = :id_Postulant")
    Optional<Announcement_Postulant> FindById(@Param("id_Postulant") long id_Postulant);

//    @Modifying
//    @Transactional
//    @Query("update Announcement_Postulant item set item.Status = :Status, item.person = :person, item.career = :career where item.id_Postulant = :id_Postulant")
//    void UpdateByCi(@Param("id_Postulant") long id_Postulant, @Param("Status") boolean Status, @Param("person") long person, @Param("career") int career);

    @Modifying
    @Transactional
    @Query("delete from Announcement_Postulant item where item.idannpost = :id_Postulant")
    void DeleteByCi(@Param("id_Postulant") long id_Postulant);


    @Query(value = "select MAX(idannpost) from announcement_postulant", nativeQuery = true)
    long selectByIdannounpost();

//    SELECT postulant.*, person.name, career.name FROM `postulant` INNER JOIN person, career where person.ci = postulant.person AND postulant.career = career.cod_career
//    @Query("SELECT status FROM Postulantes ")
//    List<Boolean> findPersonByStatus();
//    List<Announcement_Postulant> findPostulantesByPerson_Ci(String ci);
////    List<Postulantes> findPostulantesByPerson_Ci(String ci);
//    List<Announcement_Postulant> findByCareer_CodCareerOrderByPerson(int career);
//
//    List<Announcement_Postulant> queryByPerson_CiAndCareer_CodCareer(String ci, int codCareer);
//
//    @Query("SELECT status FROM Announcement_Postulant ")
//    List<Announcement_Postulant> find();
}
