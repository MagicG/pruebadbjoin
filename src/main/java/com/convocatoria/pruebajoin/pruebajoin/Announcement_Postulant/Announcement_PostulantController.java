package com.convocatoria.pruebajoin.pruebajoin.Announcement_Postulant;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class Announcement_PostulantController {

    @Autowired
    private Announcement_PostulantService announcementPostulantService;

    @RequestMapping("/announPostulant")
    public List<Announcement_Postulant> getAllAnnounPostulant() {
        return announcementPostulantService.getAllAnnounPostulant();
    }

    @RequestMapping("/announPostulant/{id}")
    public Announcement_Postulant getAnnounPostulant(@PathVariable long id) {
        return announcementPostulantService.getAnnounPostulant(id);
    }

    @PostMapping("/announPostulant")
    public long addAnnounPostulant(@RequestBody Announcement_Postulant announcementPostulant) {
        return announcementPostulantService.addAnnounPostulant(announcementPostulant);

    }

    @DeleteMapping("/announPostulant/{id}")
    public void deleteAnnounPostulant(@PathVariable long id) {
        announcementPostulantService.deleteAnnounPostulant(id);
    }

//    @RequestMapping("/announPostulant/listado/{ci}")
//    public List<Announcement_Postulant> listadoJoin(@PathVariable("ci") String ci) {
//        return announcementPostulantService.listadoJoin(ci);
//    }

//    @RequestMapping("/announPostulant/listar/{career}")
//    public List<Announcement_Postulant> listadoPorCarrera(@PathVariable("career") int career) {
//        return announcementPostulantService.listadoPorCarrera(career);
//    }
//
//    @RequestMapping("/announPostulant/listando/{ci}/{career}")
//    public List<Announcement_Postulant> QueryCiCodCareer(@PathVariable("ci") String ci , @PathVariable("career") int career) {
//        return announcementPostulantService.QueryCiCodCareer(ci,career);
//    }
}
