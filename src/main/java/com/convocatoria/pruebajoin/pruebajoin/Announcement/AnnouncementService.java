package com.convocatoria.pruebajoin.pruebajoin.Announcement;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnnouncementService {


    @Autowired
    private AnnouncementRepository announcementRepository;


    public List<Announcement> getAllAnnouncements() {
        List<Announcement> postulantes = new ArrayList<>();
        announcementRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public Announcement getAnnouncement(long id) {
        return announcementRepository.FindById(id)
                .orElse(null);

    }

    public long addAnnouncement(Announcement announcement) {
        announcementRepository.save(announcement);
        return announcementRepository.selectByIdAnnouncement();
    }

    public void deleteAnnouncement(long id) {
        announcementRepository.DeleteByCi(id);
    }


//    public List<Announcement> listadoJoin(String ci){
//
//        return announcementRepository.findPostulantesByPerson_Ci(ci);
//    }
//
//    public List<Announcement> listadoPorCarrera(int career){
//
//        return announcementRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<Announcement> QueryCiCodCareer(String ci, int career){
//
//        return announcementRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }


}
