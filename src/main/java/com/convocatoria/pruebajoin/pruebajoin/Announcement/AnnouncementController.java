package com.convocatoria.pruebajoin.pruebajoin.Announcement;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class AnnouncementController {

    @Autowired
    private AnnouncementService announcementService;

    @RequestMapping("/announcement")
    public List<Announcement> getAllAnnouncement() {
        return announcementService.getAllAnnouncements();
    }

    @RequestMapping("/announcement/{id}")
    public Announcement getAnnouncement(@PathVariable long id) {
        return announcementService.getAnnouncement(id);
    }

    @PostMapping("/announcement")
    public long addAnnouncement(@RequestBody Announcement announcement) {
        return announcementService.addAnnouncement(announcement);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/announcement/{id}")
    public void deleteAnnouncement(@PathVariable long id) {
        announcementService.deleteAnnouncement(id);
    }

//    @RequestMapping("/announcement/listado/{ci}")
//    public List<Announcement> listadoJoin(@PathVariable("ci") String ci) {
//        return announcementService.listadoJoin(ci);
//    }
//
//    @RequestMapping("/announcement/listar/{career}")
//    public List<Announcement> listadoPorCarrera(@PathVariable("career") int career) {
//        return announcementService.listadoPorCarrera(career);
//    }
//
//    @RequestMapping("/announcement/listando/{ci}/{career}")
//    public List<Announcement> QueryCiCodCareer(@PathVariable("ci") String ci , @PathVariable("career") int career) {
//        return announcementService.QueryCiCodCareer(ci,career);
//    }

}
