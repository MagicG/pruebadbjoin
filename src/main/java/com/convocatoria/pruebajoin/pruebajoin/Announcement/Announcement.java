package com.convocatoria.pruebajoin.pruebajoin.Announcement;

import com.convocatoria.pruebajoin.pruebajoin.AcademicUnit.AcademicUnit;
import com.convocatoria.pruebajoin.pruebajoin.Area.Area;
import com.convocatoria.pruebajoin.pruebajoin.Management.Management;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "Announcement")
public class Announcement {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idannouncement")
    private long idannouncement;
    @Column(name = "title", length = 100)
    private String title;
    @Column(name = "description", length = 500, nullable = false)
    private String description;
    @Column
    private Date deadline;
    @Column(name = "courtsDescription", length = 1000)
    private String courtsDescription;
    @Column(name = "appointment", length = 800)
    private String appointment;

//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idarea", referencedColumnName = "idarea")
//    @Column
    private Area area;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idmanagement", referencedColumnName = "idmanagement")
//    @Column
    private Management management;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idacademicunit", referencedColumnName = "idacademicunit")
    private AcademicUnit academicUnit;

//
//    @ManyToOne
//    @JoinColumn(name="id_career")
//    private Career career;

    public Announcement() {
    }

    public Announcement(String title, String description, Date deadline, String courtsDescription, String appointment, Area area, Management management, AcademicUnit academicUnit) {
        this.title = title;
        this.description = description;
        this.deadline = deadline;
        this.courtsDescription = courtsDescription;
        this.appointment = appointment;
        this.area = area;
        this.management = management;
        this.academicUnit = academicUnit;
    }

    public long getIdannouncement() {
        return idannouncement;
    }

    public void setIdannouncement(long idannouncement) {
        this.idannouncement = idannouncement;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getCourtsDescription() {
        return courtsDescription;
    }

    public void setCourtsDescription(String courtsDescription) {
        this.courtsDescription = courtsDescription;
    }

    public String getAppointment() {
        return appointment;
    }

    public void setAppointment(String appointment) {
        this.appointment = appointment;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Management getManagement() {
        return management;
    }

    public void setManagement(Management management) {
        this.management = management;
    }

    public AcademicUnit getAcademicUnit() {
        return academicUnit;
    }

    public void setAcademicUnit(AcademicUnit academicUnit) {
        this.academicUnit = academicUnit;
    }
}
