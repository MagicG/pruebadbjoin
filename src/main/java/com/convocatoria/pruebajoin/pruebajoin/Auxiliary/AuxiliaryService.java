package com.convocatoria.pruebajoin.pruebajoin.Auxiliary;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuxiliaryService {

    @Autowired
    private AuxiliaryRepository auxiliaryRepository;


    public List<Auxiliary> getAllAuxiliary() {
        List<Auxiliary> postulantes = new ArrayList<>();
        auxiliaryRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public Auxiliary getAuxiliary(long id) {
        return auxiliaryRepository.FindById(id)
                .orElse(null);

    }

    public long addAuxiliary(Auxiliary auxiliary) {
        auxiliaryRepository.save(auxiliary);
        return auxiliaryRepository.selectByIdauxiliary();
    }


    public void deleteAuxiliary(long id) {
        auxiliaryRepository.DeleteByCi(id);
    }


//    public List<Auxiliary> listadoPorArea(long ci){
//        return auxiliaryRepository.findAuxiliaryByArea_Idarea(ci);
//    }

    public List<Auxiliary> listadoByIdAnnouncement(long ci){ return auxiliaryRepository.findAuxiliaryByAnnouncement_Idannouncement(ci); }
//
//    public List<Auxiliary> listadoPorCarrera(int career){
//
//        return auxiliaryRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<Auxiliary> QueryCiCodCareer(String ci, int career){
//
//        return auxiliaryRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }
}
