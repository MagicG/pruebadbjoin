package com.convocatoria.pruebajoin.pruebajoin.Auxiliary;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class AuxiliaryController {

    @Autowired
    private AuxiliaryService auxiliaryService;

    @RequestMapping("/auxiliary")
    public List<Auxiliary> getAllAuxiliary() {
        return auxiliaryService.getAllAuxiliary();
    }

    @RequestMapping("/auxiliary/{id}")
    public Auxiliary getAuxiliary(@PathVariable long id) {
        return auxiliaryService.getAuxiliary(id);
    }

    @PostMapping("/auxiliary")
    public long addAuxiliary(@RequestBody Auxiliary auxiliary) {
        return auxiliaryService.addAuxiliary(auxiliary);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/auxiliary/{id}")
    public void deleteAuxiliary(@PathVariable long id) {
        auxiliaryService.deleteAuxiliary(id);
    }

//    @RequestMapping("/auxiliary/listado/{ci}")
//    public List<Auxiliary> listadoPorArea(@PathVariable("ci") long ci) {
//        return auxiliaryService.listadoPorArea(ci);
//    }

    @RequestMapping("/auxiliary/listado/announcement/{id}")
    public List<Auxiliary> listadoByIdAnnouncement(@PathVariable("id") long id){ return auxiliaryService.listadoByIdAnnouncement(id); }
//
//    @RequestMapping("/auxiliary/listar/{career}")
//    public List<Auxiliary> listadoPorCarrera(@PathVariable("career") int career) {
//        return auxiliaryService.listadoPorCarrera(career);
//    }
//
//    @RequestMapping("/auxiliary/listando/{ci}/{career}")
//    public List<Auxiliary> QueryCiCodCareer(@PathVariable("ci") String ci , @PathVariable("career") int career) {
//        return auxiliaryService.QueryCiCodCareer(ci,career);
//    }

//    @RequestMapping("/postulantes/listando/status")
//    public List<Boolean> getStatusFromPostulants() {
//        return postulantesService.getStatusFromPostulants();
//    }

}
