package com.convocatoria.pruebajoin.pruebajoin.Auxiliary;

//import com.tis.convocatorias.postulant.service.Career.Career;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface AuxiliaryRepository extends JpaRepository<Auxiliary, Long>{

    @Query("select item from Auxiliary item where item.idauxiliary = :id_Postulant")
    Optional<Auxiliary> FindById(@Param("id_Postulant") long id_Postulant);

//    @Modifying
//    @Transactional
//    @Query("update Auxiliary item set item.Status = :Status, item.person = :person, item.career = :career where item.id_Postulant = :id_Postulant")
//    void UpdateByCi(@Param("id_Postulant") long id_Postulant, @Param("Status") boolean Status, @Param("person") long person, @Param("career") int career);

    @Modifying
    @Transactional
    @Query("delete from Auxiliary item where item.idauxiliary = :id_Postulant")
    void DeleteByCi(@Param("id_Postulant") long id_Postulant);


    @Query(value = "select MAX(idauxiliary) from auxiliary", nativeQuery = true)
    long selectByIdauxiliary();

//    List<Auxiliary> findPostulantesByPerson_Ci(String ci);
//    List<Auxiliary> findAuxiliaryByArea_Idarea(long id);

    List<Auxiliary> findAuxiliaryByAnnouncement_Idannouncement(long id_announ);

}
