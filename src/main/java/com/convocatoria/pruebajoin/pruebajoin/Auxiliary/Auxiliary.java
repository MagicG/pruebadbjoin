package com.convocatoria.pruebajoin.pruebajoin.Auxiliary;

import com.convocatoria.pruebajoin.pruebajoin.AcademicUnit.AcademicUnit;
import com.convocatoria.pruebajoin.pruebajoin.Announcement.Announcement;
import com.convocatoria.pruebajoin.pruebajoin.Area.Area;
import com.convocatoria.pruebajoin.pruebajoin.Career.Career;
import com.convocatoria.pruebajoin.pruebajoin.Person.Person;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "Auxiliary")
public class Auxiliary {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idauxiliary")
    private long idauxiliary;
    @Column
    private String name;
    @Column
    private String code;
    @Column
    private String academicHours;

//    @ManyToOne(fetch = FetchType.EAGER, optional = false)


    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idannouncement", referencedColumnName = "idannouncement")
//    @Column
    private Announcement announcement;

//
//    @ManyToOne
//    @JoinColumn(name="id_career")
//    private Career career;

    public Auxiliary() {
    }

    public Auxiliary(String name, String code, String academicHours, Announcement announcement) {
        this.name = name;
        this.code = code;
        this.academicHours = academicHours;
        this.announcement = announcement;
    }

    public long getIdauxiliary() {
        return idauxiliary;
    }

    public void setIdauxiliary(long idauxiliary) {
        this.idauxiliary = idauxiliary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAcademicHours() {
        return academicHours;
    }

    public void setAcademicHours(String academicHours) {
        this.academicHours = academicHours;
    }

    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }
}
