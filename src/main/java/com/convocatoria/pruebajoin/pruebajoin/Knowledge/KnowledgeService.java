package com.convocatoria.pruebajoin.pruebajoin.Knowledge;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class KnowledgeService {

    @Autowired
    private KnowledgeRepository knowledgeRepository;


    public List<Knowledge> getAllKnowledge() {
        List<Knowledge> postulantes = new ArrayList<>();
        knowledgeRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public Knowledge getKnowledge(long id) {
        return knowledgeRepository.FindById(id)
                .orElse(null);

    }

    public long addKnowledge(Knowledge knowledge) {
        knowledgeRepository.save(knowledge);
        return knowledgeRepository.selectByIdknowledge();
    }


    public void deleteKnowledge(long id) {
        knowledgeRepository.DeleteByCi(id);
    }


    public List<Knowledge> KnowledgeByAnnoun(long id){
        return knowledgeRepository.findKnowledgeByAnnouncement_Idannouncement(id);
    }
//
//    public List<Auxiliary> listadoPorCarrera(int career){
//
//        return auxiliaryRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<Auxiliary> QueryCiCodCareer(String ci, int career){
//
//        return auxiliaryRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }
}
