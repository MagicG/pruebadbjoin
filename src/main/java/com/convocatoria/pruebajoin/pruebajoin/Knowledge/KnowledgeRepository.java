package com.convocatoria.pruebajoin.pruebajoin.Knowledge;

//import com.tis.convocatorias.postulant.service.Career.Career;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import javax.validation.constraints.Max;
import java.util.List;
import java.util.Optional;

public interface KnowledgeRepository extends JpaRepository<Knowledge, Long>{

    @Query("select item from Knowledge item where item.idknowledge = :id_Postulant")
    Optional<Knowledge> FindById(@Param("id_Postulant") long id_Postulant);

//    @Modifying
//    @Transactional
//    @Query("update Auxiliary item set item.Status = :Status, item.person = :person, item.career = :career where item.id_Postulant = :id_Postulant")
//    void UpdateByCi(@Param("id_Postulant") long id_Postulant, @Param("Status") boolean Status, @Param("person") long person, @Param("career") int career);

    @Modifying
    @Transactional
    @Query("delete from Knowledge item where item.idknowledge = :id_Postulant")
    void DeleteByCi(@Param("id_Postulant") long id_Postulant);


    @Query(value = "select MAX(idknowledge) from knowledge", nativeQuery = true)
    long selectByIdknowledge();


//    List<Auxiliary> findPostulantesByPerson_Ci(String ci);

    List<Knowledge> findKnowledgeByAnnouncement_Idannouncement(long idann);
}
