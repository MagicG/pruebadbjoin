package com.convocatoria.pruebajoin.pruebajoin.Knowledge;

import com.convocatoria.pruebajoin.pruebajoin.AcademicUnit.AcademicUnit;
import com.convocatoria.pruebajoin.pruebajoin.Announcement.Announcement;
import com.convocatoria.pruebajoin.pruebajoin.Area.Area;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "Knowledge")
public class Knowledge {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idknowledge")
    private long idknowledge;
    @Column(length = 500)
    private String description;
    @Column
    private int baseScore;
    @Column
    private int finalScore;

//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idannouncement", referencedColumnName = "idannouncement")
//    @Column
    private Announcement announcement;


//
//    @ManyToOne
//    @JoinColumn(name="id_career")
//    private Career career;

    public Knowledge() {
    }

    public Knowledge(String description, int baseScore, int finalScore, Announcement announcement) {
        this.description = description;
        this.baseScore = baseScore;
        this.finalScore = finalScore;
        this.announcement = announcement;
    }

    public long getIdknowledge() {
        return idknowledge;
    }

    public void setIdknowledge(long idknowledge) {
        this.idknowledge = idknowledge;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getBaseScore() {
        return baseScore;
    }

    public void setBaseScore(int baseScore) {
        this.baseScore = baseScore;
    }

    public int getFinalScore() {
        return finalScore;
    }

    public void setFinalScore(int finalScore) {
        this.finalScore = finalScore;
    }

    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }
}
