package com.convocatoria.pruebajoin.pruebajoin.Knowledge;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class KnowledgeController {

    @Autowired
    private KnowledgeService knowledgeService;

    @RequestMapping("/knowledge")
    public List<Knowledge> getAllKnowledge() {
        return knowledgeService.getAllKnowledge();
    }

    @RequestMapping("/knowledge/{id}")
    public Knowledge getKnowledge(@PathVariable long id) {
        return knowledgeService.getKnowledge(id);
    }

    @PostMapping("/knowledge")
    public long addKnowledge(@RequestBody Knowledge knowledge) {
        return knowledgeService.addKnowledge(knowledge);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/knowledge/{id}")
    public void deleteKnowledge(@PathVariable long id) {
        knowledgeService.deleteKnowledge(id);
    }

    @RequestMapping("/knowledge/obtain/{ci}")
    public List<Knowledge> KnowledgeByAnnoun(@PathVariable("ci") long ci) {
        return knowledgeService.KnowledgeByAnnoun(ci);
    }
//
//    @RequestMapping("/auxiliary/listar/{career}")
//    public List<Auxiliary> listadoPorCarrera(@PathVariable("career") int career) {
//        return auxiliaryService.listadoPorCarrera(career);
//    }
//
//    @RequestMapping("/auxiliary/listando/{ci}/{career}")
//    public List<Auxiliary> QueryCiCodCareer(@PathVariable("ci") String ci , @PathVariable("career") int career) {
//        return auxiliaryService.QueryCiCodCareer(ci,career);
//    }

//    @RequestMapping("/postulantes/listando/status")
//    public List<Boolean> getStatusFromPostulants() {
//        return postulantesService.getStatusFromPostulants();
//    }

}
