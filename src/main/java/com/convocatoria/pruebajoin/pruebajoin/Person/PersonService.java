package com.convocatoria.pruebajoin.pruebajoin.Person;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public List<Person> getAllPersons() {
        //return postulants;
        List<Person> people = new ArrayList<>();
        personRepository.findAll()
                .forEach(people::add);
        return people;
    }

    public Person getPerson(String ci) {
        //return postulants.stream().filter(t -> t.getId().equals(id)).findFirst().get();
//        String aux = id.toString();
//        return postulantRepository.getOne(id);

        return personRepository.FindByCi(ci)
                .orElse(null);
    }


    public void addPerson(Person person) {

        personRepository.save(person);
    }

    public void updatePerson(String ci, Person person) {
        personRepository.UpdateByCi(ci, person.getName(), person.getLastName(), person.getAddress(), person.getPhoneNumber(), person.getEmail());

    }

    public void deletePerson(String ci) {
        //postulants.removeIf(p -> p.getId().equals(id));
        personRepository.DeleteByCi(ci);
    }

    public Object encontrarci(String ci){
        return personRepository.encontrarci(ci);

    }

}
