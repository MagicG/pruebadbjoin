package com.convocatoria.pruebajoin.pruebajoin.Person;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class PersonController {

    @Autowired
    private PersonService personService;

    @RequestMapping("/person")
    public List<Person> getAllPerson() {
        return personService.getAllPersons();
    }

    @RequestMapping("/person/{id}")
    public Person getPerson(@PathVariable String id) {
        return personService.getPerson(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/person")
    public long addPerson(@RequestBody Person person) {
        personService.addPerson(person);
        return person.getId();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/person/{id}")
    public void updatePerson(@RequestBody Person person, @PathVariable String id) {
        personService.updatePerson(id, person);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/person/{id}")
    public void deletePerson(@PathVariable String id) {
        personService.deletePerson(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/person/encontrar/{ci}")
    public Object encontrarci(@PathVariable String ci) { return personService.encontrarci(ci); }
}
