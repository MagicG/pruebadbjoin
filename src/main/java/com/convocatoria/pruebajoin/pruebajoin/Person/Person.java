package com.convocatoria.pruebajoin.pruebajoin.Person;

import com.convocatoria.pruebajoin.pruebajoin.Postulantes.Postulantes;
import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "Person")
//@PrimaryKeyJoinColumn(name = "id_person", referencedColumnName = "id_person")

public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
//    @Column(name = "id", updatable = false, nullable = false
//    @Transient
    @Column(name="idperson")
    private long id;
    @Column(name="ci")
    private String ci;
    @Column
    private String name;
    @Column(name = "surname")
    private String lastName;
    @Column
    private String address;
    @Column(name = "phone")
    private int phoneNumber;
    @Column
    private String email;

//    @OneToMany(targetEntity=Postulantes.class, mappedBy = "person")
//    private List<Postulantes> postulantes;

    public Person() {
    }

    public Person(String ci, String name, String lastName, String address, int phoneNumber, String email) {
        this.ci = ci;
        this.name = name;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
//        this.postulantes = postulantes;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

//    public List<Postulantes> getPostulantes() {
//        return postulantes;
//    }
//
//    public void setPostulantes(List<Postulantes> postulantes) {
//        this.postulantes = postulantes;
//    }
}
