package com.convocatoria.pruebajoin.pruebajoin.Area;//package com.tis.convocatorias.postulant.service.Career;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AreaService {

    @Autowired
    private AreaRepository areaRepository;

    public List<Area> getAllAreas() {
        //return postulants;
        List<Area> people = new ArrayList<>();
        areaRepository.findAll()
                .forEach(people::add);
        return people;
    }

    public Area getArea(int ci) {
        //return postulants.stream().filter(t -> t.getId().equals(id)).findFirst().get();
//        String aux = id.toString();
//        return postulantRepository.getOne(id);

        return areaRepository.FindByCi(ci)
                .orElse(null);
    }


    public void addArea(Area area) {

        areaRepository.save(area);
    }

//    public void updateArea(int ci, Area area) {
//        areaRepository.UpdateByCi(ci, area.getName());
//
//    }

    public void deleteArea(int ci) {
        //postulants.removeIf(p -> p.getId().equals(id));
        areaRepository.DeleteByCi(ci);
    }
}
