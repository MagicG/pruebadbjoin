package com.convocatoria.pruebajoin.pruebajoin.Area;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;

public interface AreaRepository extends JpaRepository<Area, Long> {

    @Query("select item from Area item where item.idarea = :codCareer")
    Optional<Area> FindByCi(@Param("codCareer") int codCareer);

//    @Modifying
//    @Transactional
//    @Query("update Area item set item.name = :name where item.codCareer = :codCareer")
//    void UpdateByCi(@Param("codCareer") int codCareer, @Param("name") String name);

    @Modifying
    @Transactional
    @Query("delete from Area item where item.idarea = :codCareer")
    void DeleteByCi(@Param("codCareer") int id_career);
}
