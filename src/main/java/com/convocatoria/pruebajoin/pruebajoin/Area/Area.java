package com.convocatoria.pruebajoin.pruebajoin.Area;//package com.tis.convocatorias.postulant.service.Career;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "Area")
public class Area {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
////    @Column(name = "id", updatable = false, nullable = false
//    @Transient
    @Column(name="idarea")
    private long idarea;

    @Column
    private String name;



    public Area() {
    }

    public Area( String name) {

        this.name = name;
    }

    public long getIdarea() {
        return idarea;
    }

    public void setIdarea(long idarea) {
        this.idarea = idarea;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
