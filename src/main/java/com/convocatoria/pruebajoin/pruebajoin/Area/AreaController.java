package com.convocatoria.pruebajoin.pruebajoin.Area;//package com.tis.convocatorias.postulant.service.Career;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class AreaController {

    @Autowired
    private AreaService areaService;

    @RequestMapping("/area")
    public List<Area> getAllAreas() {
        return areaService.getAllAreas();
    }

    @RequestMapping("/area/{id}")
    public Area getArea(@PathVariable int id) {
        return areaService.getArea(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/area")
    public long addArea(@RequestBody Area area) {
        areaService.addArea(area);
        return area.getIdarea();
    }

//    @RequestMapping(method = RequestMethod.PUT, value = "/area/{id}")
//    public void updateArea(@RequestBody Area area, @PathVariable int id) {
//        areaService.updateArea(id, area);
//    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/area/{id}")
    public void deleteArea(@PathVariable int id) {
        areaService.deleteArea(id);
    }
}
