package com.convocatoria.pruebajoin.pruebajoin.PointssubsubdetailMerit;

import com.convocatoria.pruebajoin.pruebajoin.AcademicUnit.AcademicUnit;
import com.convocatoria.pruebajoin.pruebajoin.Area.Area;
import com.convocatoria.pruebajoin.pruebajoin.SubsubdetailMerit.SubsubdetailMerit;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "PointssubsubdetailMerit")
public class PointssubsubdetailMerit {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idpointssubsubdetailmerit")
    private long idpointssubsubdetailmerit;
    @Column
    private String description;


//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idsubsubdetailmerit", referencedColumnName = "idsubsubdetailmerit")
//    @Column
    private SubsubdetailMerit subsubdetailMerit;


    public PointssubsubdetailMerit() {
    }

    public PointssubsubdetailMerit(String description, SubsubdetailMerit subsubdetailMerit) {
        this.description = description;
        this.subsubdetailMerit = subsubdetailMerit;
    }

    public long getIdpointssubsubdetailmerit() {
        return idpointssubsubdetailmerit;
    }

    public void setIdpointssubsubdetailmerit(long idpointssubsubdetailmerit) {
        this.idpointssubsubdetailmerit = idpointssubsubdetailmerit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SubsubdetailMerit getSubsubdetailMerit() {
        return subsubdetailMerit;
    }

    public void setSubsubdetailMerit(SubsubdetailMerit subsubdetailMerit) {
        this.subsubdetailMerit = subsubdetailMerit;
    }
}
