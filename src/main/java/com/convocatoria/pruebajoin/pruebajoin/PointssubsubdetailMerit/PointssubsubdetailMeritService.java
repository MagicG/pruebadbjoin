package com.convocatoria.pruebajoin.pruebajoin.PointssubsubdetailMerit;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PointssubsubdetailMeritService {

    @Autowired
    private PointssubsubdetailMeritRepository pointssubsubdetailMeritRepository;


    public List<PointssubsubdetailMerit> getAllPointssubsubdetailMerit() {
        List<PointssubsubdetailMerit> postulantes = new ArrayList<>();
        pointssubsubdetailMeritRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public PointssubsubdetailMerit getPointssubsubdetailMerit(long id) {
        return pointssubsubdetailMeritRepository.FindById(id)
                .orElse(null);

    }

    public long addPointssubsubdetailMerit(PointssubsubdetailMerit pointssubsubdetailMerit) {
        pointssubsubdetailMeritRepository.save(pointssubsubdetailMerit);
        return pointssubsubdetailMeritRepository.selectByIdpointssubsubdetailmerit();
    }


    public void deletePointssubsubdetailMerit(long id) {
        pointssubsubdetailMeritRepository.DeleteByCi(id);
    }


//    public List<Auxiliary> listadoJoin(String ci){
//        return auxiliaryRepository.findPostulantesByPerson_Ci(ci);
//    }
//
//    public List<Auxiliary> listadoPorCarrera(int career){
//
//        return auxiliaryRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<Auxiliary> QueryCiCodCareer(String ci, int career){
//
//        return auxiliaryRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }
}
