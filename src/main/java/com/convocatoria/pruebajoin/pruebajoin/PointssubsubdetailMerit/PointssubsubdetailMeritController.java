package com.convocatoria.pruebajoin.pruebajoin.PointssubsubdetailMerit;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class PointssubsubdetailMeritController {

    @Autowired
    private PointssubsubdetailMeritService pointssubsubdetailMeritService;

    @RequestMapping("/pointssubsubdetailmerit")
    public List<PointssubsubdetailMerit> getAllPointssubsubdetailMerit() {
        return pointssubsubdetailMeritService.getAllPointssubsubdetailMerit();
    }

    @RequestMapping("/pointssubsubdetailmerit/{id}")
    public PointssubsubdetailMerit getPointssubsubdetailMerit(@PathVariable long id) {
        return pointssubsubdetailMeritService.getPointssubsubdetailMerit(id);
    }

    @PostMapping("/pointssubsubdetailmerit")
    public long addPointssubsubdetailMerit(@RequestBody PointssubsubdetailMerit pointssubsubdetailMerit) {
        return pointssubsubdetailMeritService.addPointssubsubdetailMerit(pointssubsubdetailMerit);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/pointssubsubdetailmerit/{id}")
    public void deletePointssubsubdetailMerit(@PathVariable long id) {
        pointssubsubdetailMeritService.deletePointssubsubdetailMerit(id);
    }

//    @RequestMapping("/auxiliary/listado/{ci}")
//    public List<Auxiliary> listadoJoin(@PathVariable("ci") String ci) {
//        return auxiliaryService.listadoJoin(ci);
//    }
//
//    @RequestMapping("/auxiliary/listar/{career}")
//    public List<Auxiliary> listadoPorCarrera(@PathVariable("career") int career) {
//        return auxiliaryService.listadoPorCarrera(career);
//    }
//
//    @RequestMapping("/auxiliary/listando/{ci}/{career}")
//    public List<Auxiliary> QueryCiCodCareer(@PathVariable("ci") String ci , @PathVariable("career") int career) {
//        return auxiliaryService.QueryCiCodCareer(ci,career);
//    }

//    @RequestMapping("/postulantes/listando/status")
//    public List<Boolean> getStatusFromPostulants() {
//        return postulantesService.getStatusFromPostulants();
//    }

}
