package com.convocatoria.pruebajoin.pruebajoin.Results;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class ResultsController {

    @Autowired
    private ResultsService resultsService;

    @RequestMapping("/results")
    public List<Results> getAllResults() {
        return resultsService.getAllResults();
    }

    @RequestMapping("/results/{id}")
    public Results getResults(@PathVariable long id) {
        return resultsService.getResults(id);
    }

    @PostMapping("/results")
    public long addResults(@RequestBody Results results) {
        return resultsService.addResults(results);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/results/{id}")
    public void deleteResults(@PathVariable long id) {
        resultsService.deleteResults(id);
    }

//    @RequestMapping("/auxiliary/listado/{ci}")
//    public List<Auxiliary> listadoJoin(@PathVariable("ci") String ci) {
//        return auxiliaryService.listadoJoin(ci);
//    }
//
//    @RequestMapping("/auxiliary/listar/{career}")
//    public List<Auxiliary> listadoPorCarrera(@PathVariable("career") int career) {
//        return auxiliaryService.listadoPorCarrera(career);
//    }
//
//    @RequestMapping("/auxiliary/listando/{ci}/{career}")
//    public List<Auxiliary> QueryCiCodCareer(@PathVariable("ci") String ci , @PathVariable("career") int career) {
//        return auxiliaryService.QueryCiCodCareer(ci,career);
//    }

//    @RequestMapping("/postulantes/listando/status")
//    public List<Boolean> getStatusFromPostulants() {
//        return postulantesService.getStatusFromPostulants();
//    }

}
