package com.convocatoria.pruebajoin.pruebajoin.Results;

import com.convocatoria.pruebajoin.pruebajoin.AcademicUnit.AcademicUnit;
import com.convocatoria.pruebajoin.pruebajoin.Announcement.Announcement;
import com.convocatoria.pruebajoin.pruebajoin.Area.Area;
import com.convocatoria.pruebajoin.pruebajoin.Auxiliary.Auxiliary;
import com.convocatoria.pruebajoin.pruebajoin.Postulantes.Postulantes;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "Results")
public class Results {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idresults")
    private long idresults;
    @Column
    private float score;
    @Column
    private boolean status;


//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idauxiliary", referencedColumnName = "idauxiliary")
//    @Column
    private Auxiliary auxiliary;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idannouncement", referencedColumnName = "idannouncement")
//    @Column
    private Announcement announcement;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idpostulant", referencedColumnName = "idpostulant")
//    @Column
    private Postulantes postulantes;

//
//    @ManyToOne
//    @JoinColumn(name="id_career")
//    private Career career;

    public Results() {
    }

    public Results(float score, boolean status, Auxiliary auxiliary, Announcement announcement, Postulantes postulantes) {
        this.score = score;
        this.status = status;
        this.auxiliary = auxiliary;
        this.announcement = announcement;
        this.postulantes = postulantes;
    }

    public long getIdresults() {
        return idresults;
    }

    public void setIdresults(long idresults) {
        this.idresults = idresults;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Auxiliary getAuxiliary() {
        return auxiliary;
    }

    public void setAuxiliary(Auxiliary auxiliary) {
        this.auxiliary = auxiliary;
    }

    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }

    public Postulantes getPostulantes() {
        return postulantes;
    }

    public void setPostulantes(Postulantes postulantes) {
        this.postulantes = postulantes;
    }
}
