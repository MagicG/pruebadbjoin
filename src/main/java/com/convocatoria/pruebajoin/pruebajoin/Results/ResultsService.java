package com.convocatoria.pruebajoin.pruebajoin.Results;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ResultsService {

    @Autowired
    private ResultsRepository resultsRepository;


    public List<Results> getAllResults() {
        List<Results> postulantes = new ArrayList<>();
        resultsRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public Results getResults(long id) {
        return resultsRepository.FindById(id)
                .orElse(null);

    }

    public long addResults(Results results) {
        resultsRepository.save(results);
        return resultsRepository.selectByIdresults();
    }


    public void deleteResults(long id) {
        resultsRepository.DeleteByCi(id);
    }


//    public List<Auxiliary> listadoJoin(String ci){
//        return auxiliaryRepository.findPostulantesByPerson_Ci(ci);
//    }
//
//    public List<Auxiliary> listadoPorCarrera(int career){
//
//        return auxiliaryRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<Auxiliary> QueryCiCodCareer(String ci, int career){
//
//        return auxiliaryRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }
}
