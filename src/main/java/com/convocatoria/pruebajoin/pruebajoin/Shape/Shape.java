package com.convocatoria.pruebajoin.pruebajoin.Shape;

import com.convocatoria.pruebajoin.pruebajoin.Announcement.Announcement;
import com.convocatoria.pruebajoin.pruebajoin.Auxiliary.Auxiliary;
import com.convocatoria.pruebajoin.pruebajoin.Postulantes.Postulantes;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "Shape")
public class Shape {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idshape")
    private long idshape;
    @Column
    private String description;


    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idannouncement", referencedColumnName = "idannouncement")
//    @Column
    private Announcement announcement;

//
//    @ManyToOne
//    @JoinColumn(name="id_career")
//    private Career career;

    public Shape() {
    }

    public Shape(String description, Announcement announcement) {
        this.description = description;
        this.announcement = announcement;
    }

    public long getIdshape() {
        return idshape;
    }

    public void setIdshape(long idshape) {
        this.idshape = idshape;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }
}
