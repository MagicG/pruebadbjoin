package com.convocatoria.pruebajoin.pruebajoin.Shape;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ShapeService {



    @Autowired
    private ShapeRepository shapeRepository;


    public List<Shape> getAllShape() {
        List<Shape> postulantes = new ArrayList<>();
        shapeRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public Shape getShape(long id) {
        return shapeRepository.FindById(id)
                .orElse(null);

    }

    public long addShape(Shape shape) {
        shapeRepository.save(shape);
        return shapeRepository.selectByIdshape();
    }

    public void deleteShape(long id) {
        shapeRepository.DeleteByCi(id);
    }


//    public List<Shape> listadoAnnouncement(long id){
//
//        return shapeRepository.findLogBookByAnnouncement_Idannouncement(id);
//    }
//
//    public List<LogBook> listadoPorCarrera(int career){
//
//        return logBookRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<LogBook> QueryCiCodCareer(String ci, int career){
//
//        return logBookRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }

}
