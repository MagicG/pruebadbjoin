package com.convocatoria.pruebajoin.pruebajoin.Shape;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class ShapeController {

    @Autowired
    private ShapeService shapeService;

    @RequestMapping("/shape")
    public List<Shape> getAllShape() {
        return shapeService.getAllShape();
    }

    @RequestMapping("/shape/{id}")
    public Shape getShape(@PathVariable long id) {
        return shapeService.getShape(id);
    }

    @PostMapping("/shape")
    public long addShape(@RequestBody Shape shape) {
        return shapeService.addShape(shape);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/shape/{id}")
    public void deleteShape(@PathVariable long id) {
        shapeService.deleteShape(id);
    }

//    @RequestMapping("/logbook/listado/{ci}")
//    public List<Shape> listadoAnnouncement(@PathVariable("ci") long ci) {
//        return shapeService.listadoAnnouncement(ci);
//    }
//
//    @RequestMapping("/logbook/listar/{career}")
//    public List<LogBook> listadoPorCarrera(@PathVariable("career") int career) {
//        return logBookService.listadoPorCarrera(career);
//    }
//
//    @RequestMapping("/logbook/listando/{ci}/{career}")
//    public List<LogBook> QueryCiCodCareer(@PathVariable("ci") String ci , @PathVariable("career") int career) {
//        return logBookService.QueryCiCodCareer(ci,career);
//    }

//    @RequestMapping("/postulantes/listando/status")
//    public List<Boolean> getStatusFromPostulants() {
//        return postulantesService.getStatusFromPostulants();
//    }

}
