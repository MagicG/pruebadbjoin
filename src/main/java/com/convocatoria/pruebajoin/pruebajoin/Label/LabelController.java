package com.convocatoria.pruebajoin.pruebajoin.Label;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class LabelController {

    @Autowired
    private LabelService labelService;

    @RequestMapping("/label")
    public List<Label> getAllLabel() {
        return labelService.getAllLabel();
    }

    @RequestMapping("/label/{id}")
    public Label getLabel(@PathVariable long id) {
        return labelService.getLabel(id);
    }

    @PostMapping("/label")
    public long addLabel(@RequestBody Label label) {
        return labelService.addLabel(label);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/label/{id}")
    public void deleteLabel(@PathVariable long id) {
        labelService.deleteLabel(id);
    }



    @PostMapping("/label/buscar")
    public List<Label> searchPostulantes(@RequestBody Map<String, String> body) {
        String searchTerm = body.get("text");
        String searchTerm2 = "";
        String[] currencies = searchTerm.split(" ");
        if (currencies.length > 1) {
            searchTerm = currencies[0];
            searchTerm2 = currencies[1];
            List<Label> combo = new ArrayList<>();
            combo.addAll(labelService.findBytext(searchTerm, searchTerm));
            combo.addAll(labelService.findBytext(searchTerm2, searchTerm2));
            LinkedHashSet<Label> hashSet = new LinkedHashSet<>(combo);
            ArrayList<Label> listWithoutDuplicates = new ArrayList<>(hashSet);
            return listWithoutDuplicates;
        }
        ;
        return labelService.findBytext(searchTerm, searchTerm);
    }

//    @RequestMapping("/logbook/listado/{ci}")
//    public List<Label> listadoAnnouncement(@PathVariable("ci") long ci) {
//        return labelService.listadoAnnouncement(ci);
//    }
//
//    @RequestMapping("/logbook/listar/{career}")
//    public List<LogBook> listadoPorCarrera(@PathVariable("career") int career) {
//        return logBookService.listadoPorCarrera(career);
//    }
//
//    @RequestMapping("/logbook/listando/{ci}/{career}")
//    public List<LogBook> QueryCiCodCareer(@PathVariable("ci") String ci , @PathVariable("career") int career) {
//        return logBookService.QueryCiCodCareer(ci,career);
//    }

//    @RequestMapping("/postulantes/listando/status")
//    public List<Boolean> getStatusFromPostulants() {
//        return postulantesService.getStatusFromPostulants();
//    }

}
