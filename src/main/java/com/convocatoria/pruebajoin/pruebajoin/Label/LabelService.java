package com.convocatoria.pruebajoin.pruebajoin.Label;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LabelService {



    @Autowired
    private LabelRepository labelRepository;


    public List<Label> getAllLabel() {
        List<Label> postulantes = new ArrayList<>();
        labelRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public Label getLabel(long id) {
        return labelRepository.FindById(id)
                .orElse(null);

    }

    public long addLabel(Label label) {
        labelRepository.save(label);
        return labelRepository.selectByIdlabel();
    }

    public void deleteLabel(long id) {
        labelRepository.DeleteByCi(id);
    }





    public List<Label> findBytext(String textBeta, String textAlfa){

        return labelRepository.queryByPostulantes_Person_NameContainingOrPostulantes_Person_LastNameContaining(textAlfa,textBeta);
    }


//    public List<Label> listadoAnnouncement(long id){
//
//        return labelRepository.findLogBookByAnnouncement_Idannouncement(id);
//    }
//
//    public List<LogBook> listadoPorCarrera(int career){
//
//        return logBookRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<LogBook> QueryCiCodCareer(String ci, int career){
//
//        return logBookRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }

}
