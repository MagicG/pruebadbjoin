package com.convocatoria.pruebajoin.pruebajoin.Label;

import com.convocatoria.pruebajoin.pruebajoin.Announcement.Announcement;
import com.convocatoria.pruebajoin.pruebajoin.Auxiliary.Auxiliary;
import com.convocatoria.pruebajoin.pruebajoin.Postulantes.Postulantes;
import com.convocatoria.pruebajoin.pruebajoin.Shape.Shape;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "Label")
public class Label {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idlabel")
    private long idlabel;

//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idpostulant", referencedColumnName = "idpostulant")
//    @Column
    private Postulantes postulantes;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idauxiliary", referencedColumnName = "idauxiliary")
//    @Column
    private Auxiliary auxiliary;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idshape", referencedColumnName = "idshape")
//    @Column
    private Shape shape;

//
//    @ManyToOne
//    @JoinColumn(name="id_career")
//    private Career career;

    public Label() {
    }

    public Label(Postulantes postulantes, Auxiliary auxiliary, Shape shape) {
        this.postulantes = postulantes;
        this.auxiliary = auxiliary;
        this.shape = shape;
    }

    public long getIdlabel() {
        return idlabel;
    }

    public void setIdlabel(long idlabel) {
        this.idlabel = idlabel;
    }

    public Postulantes getPostulantes() {
        return postulantes;
    }

    public void setPostulantes(Postulantes postulantes) {
        this.postulantes = postulantes;
    }

    public Auxiliary getAuxiliary() {
        return auxiliary;
    }

    public void setAuxiliary(Auxiliary auxiliary) {
        this.auxiliary = auxiliary;
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }
}
