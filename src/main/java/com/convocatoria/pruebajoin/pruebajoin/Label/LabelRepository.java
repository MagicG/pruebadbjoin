package com.convocatoria.pruebajoin.pruebajoin.Label;

//import com.tis.convocatorias.postulant.service.Career.Career;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface LabelRepository extends JpaRepository<Label, Long>{

    @Query("select item from Label item where item.idlabel = :id_Postulant")
    Optional<Label> FindById(@Param("id_Postulant") long id_Postulant);

//    @Modifying
//    @Transactional
//    @Query("update LogBook item set item.Status = :Status, item.person = :person, item.career = :career where item.id_Postulant = :id_Postulant")
//    void UpdateByCi(@Param("id_Postulant") long id_Postulant, @Param("Status") boolean Status, @Param("person") long person, @Param("career") int career);

    @Modifying
    @Transactional
    @Query("delete from Label item where item.idlabel = :id_Postulant")
    void DeleteByCi(@Param("id_Postulant") long id_Postulant);


    List<Label> queryByPostulantes_Person_NameContainingOrPostulantes_Person_LastNameContaining(String textAlfa, String textBeta);



    @Query(value = "select MAX(idlabel) from label", nativeQuery = true)
    long selectByIdlabel();

//    SELECT postulant.*, person.name, career.name FROM `postulant` INNER JOIN person, career where person.ci = postulant.person AND postulant.career = career.cod_career
//    @Query("SELECT status FROM Postulantes ")
//    List<Label> findLogBookByAnnouncement_Idannouncement(long id_announ);
//    List<LogBook> findPostulantesByPerson_Ci(String ci);
////    List<Postulantes> findPostulantesByPerson_Ci(String ci);
//    List<LogBook> findByCareer_CodCareerOrderByPerson(int career);
//
//    List<LogBook> queryByPerson_CiAndCareer_CodCareer(String ci, int codCareer);
//
//    @Query("SELECT status FROM LogBook ")
//    List<LogBook> find();
}
