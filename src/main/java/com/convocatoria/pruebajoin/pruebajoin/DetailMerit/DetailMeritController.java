package com.convocatoria.pruebajoin.pruebajoin.DetailMerit;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class DetailMeritController {

    @Autowired
    private DetailMeritService detailMeritService;

    @RequestMapping("/detailmerit")
    public List<DetailMerit> getAllDetailMerit() {
        return detailMeritService.getAllDetailMerit();
    }

    @RequestMapping("/detailmerit/{id}")
    public DetailMerit getDetailMerit(@PathVariable long id) {
        return detailMeritService.getDetailMerit(id);
    }

    @PostMapping("/detailmerit")
    public long addDetailMerit(@RequestBody DetailMerit detailMerit) {
        return detailMeritService.addDetailMerit(detailMerit);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/detailmerit/{id}")
    public void deleteDetailMerit(@PathVariable long id) {
        detailMeritService.deleteDetailMerit(id);
    }

//    @RequestMapping("/auxiliary/listado/{ci}")
//    public List<Auxiliary> listadoJoin(@PathVariable("ci") String ci) {
//        return auxiliaryService.listadoJoin(ci);
//    }
//
//    @RequestMapping("/auxiliary/listar/{career}")
//    public List<Auxiliary> listadoPorCarrera(@PathVariable("career") int career) {
//        return auxiliaryService.listadoPorCarrera(career);
//    }
//
//    @RequestMapping("/auxiliary/listando/{ci}/{career}")
//    public List<Auxiliary> QueryCiCodCareer(@PathVariable("ci") String ci , @PathVariable("career") int career) {
//        return auxiliaryService.QueryCiCodCareer(ci,career);
//    }

//    @RequestMapping("/postulantes/listando/status")
//    public List<Boolean> getStatusFromPostulants() {
//        return postulantesService.getStatusFromPostulants();
//    }

}
