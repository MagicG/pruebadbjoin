package com.convocatoria.pruebajoin.pruebajoin.DetailMerit;

import com.convocatoria.pruebajoin.pruebajoin.AcademicUnit.AcademicUnit;
import com.convocatoria.pruebajoin.pruebajoin.Area.Area;
import com.convocatoria.pruebajoin.pruebajoin.Merit.Merit;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "DetailMerit")
public class DetailMerit {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "iddetailmerit")
    private long iddetailmerit;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private int percentage;

    //    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idmerit", referencedColumnName = "idmerit")
//    @Column
    private Merit merit;

//
//    @ManyToOne
//    @JoinColumn(name="id_career")
//    private Career career;

    public DetailMerit() {
    }

    public DetailMerit(String name, String description, int percentage, Merit merit) {
        this.name = name;
        this.description = description;
        this.percentage = percentage;
        this.merit = merit;
    }

    public long getIddetailmerit() {
        return iddetailmerit;
    }

    public void setIddetailmerit(long iddetailmerit) {
        this.iddetailmerit = iddetailmerit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public Merit getMerit() {
        return merit;
    }

    public void setMerit(Merit merit) {
        this.merit = merit;
    }
}
