package com.convocatoria.pruebajoin.pruebajoin.DetailMerit;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DetailMeritService {

    @Autowired
    private DetailMeritRepository detailMeritRepository;


    public List<DetailMerit> getAllDetailMerit() {
        List<DetailMerit> postulantes = new ArrayList<>();
        detailMeritRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public DetailMerit getDetailMerit(long id) {
        return detailMeritRepository.FindById(id)
                .orElse(null);

    }

    public long addDetailMerit(DetailMerit detailMerit) {
        detailMeritRepository.save(detailMerit);
        return detailMeritRepository.selectByIddetailmerit();
    }


    public void deleteDetailMerit(long id) {
        detailMeritRepository.DeleteByCi(id);
    }


//    public List<Auxiliary> listadoJoin(String ci){
//        return auxiliaryRepository.findPostulantesByPerson_Ci(ci);
//    }
//
//    public List<Auxiliary> listadoPorCarrera(int career){
//
//        return auxiliaryRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<Auxiliary> QueryCiCodCareer(String ci, int career){
//
//        return auxiliaryRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }
}
