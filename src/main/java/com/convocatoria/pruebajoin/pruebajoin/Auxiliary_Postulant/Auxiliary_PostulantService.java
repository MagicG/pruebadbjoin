package com.convocatoria.pruebajoin.pruebajoin.Auxiliary_Postulant;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class Auxiliary_PostulantService {


    @Autowired
    private Auxiliary_PostulantRepository auxiliaryPostulantRepository;


    public List<Auxiliary_Postulant> getAllAuxPostulant() {
        List<Auxiliary_Postulant> postulantes = new ArrayList<>();
        auxiliaryPostulantRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public Auxiliary_Postulant getAuxPostulant(long id) {
        return auxiliaryPostulantRepository.FindById(id)
                .orElse(null);

    }

    public long addAuxPostulant(Auxiliary_Postulant auxiliaryPostulant) {
        auxiliaryPostulantRepository.save(auxiliaryPostulant);
        return auxiliaryPostulantRepository.selectByIdauxpostulant();
    }

    public void deleteAuxPostulant(long id) {
        auxiliaryPostulantRepository.DeleteByCi(id);
    }


//    public List<Auxiliary_Postulant> listadoJoin(String ci){
//        return auxiliaryPostulantRepository.findPostulantesByPerson_Ci(ci);
//    }
//
//    public List<Auxiliary_Postulant> listadoPorCarrera(int career){
//
//        return auxiliaryPostulantRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<Auxiliary_Postulant> QueryCiCodCareer(String ci, int career){
//
//        return auxiliaryPostulantRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }


}
