package com.convocatoria.pruebajoin.pruebajoin.Auxiliary_Postulant;

//import com.tis.convocatorias.postulant.service.Career.Career;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface Auxiliary_PostulantRepository extends JpaRepository<Auxiliary_Postulant, Long>{

    @Query("select item from Auxiliary_Postulant item where item.auxiliary = :id_Postulant")
    Optional<Auxiliary_Postulant> FindById(@Param("id_Postulant") long id_Postulant);

//    @Modifying
//    @Transactional
//    @Query("update Auxiliary_Postulant item set item.Status = :Status, item.person = :person, item.career = :career where item.id_Postulant = :id_Postulant")
//    void UpdateByCi(@Param("id_Postulant") long id_Postulant, @Param("Status") boolean Status, @Param("person") long person, @Param("career") int career);

    @Modifying
    @Transactional
    @Query("delete from Auxiliary_Postulant item where item.auxiliary = :id_Postulant")
    void DeleteByCi(@Param("id_Postulant") long id_Postulant);


    @Query(value = "select MAX(idauxpostulant) from auxiliary_postulant", nativeQuery = true)
    long selectByIdauxpostulant();

//    SELECT postulant.*, person.name, career.name FROM `postulant` INNER JOIN person, career where person.ci = postulant.person AND postulant.career = career.cod_career
//    @Query("SELECT status FROM Postulantes ")
//    List<Boolean> findPersonByStatus();
//    List<Auxiliary_Postulant> findPostulantesByPerson_Ci(String ci);
////    List<Postulantes> findPostulantesByPerson_Ci(String ci);
//    List<Auxiliary_Postulant> findByCareer_CodCareerOrderByPerson(int career);
//
//    List<Auxiliary_Postulant> queryByPerson_CiAndCareer_CodCareer(String ci, int codCareer);
//
//    @Query("SELECT status FROM Auxiliary_Postulant ")
//    List<Auxiliary_Postulant> find();
}
