package com.convocatoria.pruebajoin.pruebajoin.Auxiliary_Postulant;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class Auxiliary_PostulantController {

    @Autowired
    private Auxiliary_PostulantService auxiliaryPostulantService;

    @RequestMapping("/auxpostulant")
    public List<Auxiliary_Postulant> getAllAuxPostulant() {
        return auxiliaryPostulantService.getAllAuxPostulant();
    }

    @RequestMapping("/auxpostulant/{id}")
    public Auxiliary_Postulant getAuxPostulant(@PathVariable long id) {
        return auxiliaryPostulantService.getAuxPostulant(id);
    }

    @PostMapping("/auxpostulant")
    public long addAuxPostulant(@RequestBody Auxiliary_Postulant auxiliaryPostulant) {
        return auxiliaryPostulantService.addAuxPostulant(auxiliaryPostulant);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/auxpostulant/{id}")
    public void deleteAuxPostulant(@PathVariable long id) {
        auxiliaryPostulantService.deleteAuxPostulant(id);
    }

//    @RequestMapping("/auxpostulant/listado/{ci}")
//    public List<Auxiliary_Postulant> listadoJoin(@PathVariable("ci") String ci) {
//        return auxiliaryPostulantService.listadoJoin(ci);
//    }
//
//    @RequestMapping("/auxpostulant/listar/{career}")
//    public List<Auxiliary_Postulant> listadoPorCarrera(@PathVariable("career") int career) {
//        return auxiliaryPostulantService.listadoPorCarrera(career);
//    }
//
//    @RequestMapping("/auxpostulant/listando/{ci}/{career}")
//    public List<Auxiliary_Postulant> QueryCiCodCareer(@PathVariable("ci") String ci , @PathVariable("career") int career) {
//        return auxiliaryPostulantService.QueryCiCodCareer(ci,career);
//    }

//    @RequestMapping("/postulantes/listando/status")
//    public List<Boolean> getStatusFromPostulants() {
//        return postulantesService.getStatusFromPostulants();
//    }

}
