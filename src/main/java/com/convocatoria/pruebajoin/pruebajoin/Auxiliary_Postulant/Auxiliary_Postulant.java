package com.convocatoria.pruebajoin.pruebajoin.Auxiliary_Postulant;

import com.convocatoria.pruebajoin.pruebajoin.Auxiliary.Auxiliary;
import com.convocatoria.pruebajoin.pruebajoin.Career.Career;
import com.convocatoria.pruebajoin.pruebajoin.Person.Person;
import com.convocatoria.pruebajoin.pruebajoin.Postulantes.Postulantes;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "Auxiliary_Postulant")
public class Auxiliary_Postulant {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idauxpostulant")
    private long idauxpostulant;

//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idauxiliary", referencedColumnName = "idauxiliary")
//    @Column
    private Auxiliary auxiliary;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idpostulant", referencedColumnName = "idpostulant")
//    @Column
    private Postulantes postulantes;

//
//    @ManyToOne
//    @JoinColumn(name="id_career")
//    private Career career;

    public Auxiliary_Postulant() {
    }

    public Auxiliary_Postulant(Auxiliary auxiliary, Postulantes postulantes) {
        this.auxiliary = auxiliary;
        this.postulantes = postulantes;
    }

    public long getIdauxpostulant() {
        return idauxpostulant;
    }

    public void setIdauxpostulant(long idauxpostulant) {
        this.idauxpostulant = idauxpostulant;
    }

    public Auxiliary getAuxiliary() {
        return auxiliary;
    }

    public void setAuxiliary(Auxiliary auxiliary) {
        this.auxiliary = auxiliary;
    }

    public Postulantes getPostulantes() {
        return postulantes;
    }

    public void setPostulantes(Postulantes postulantes) {
        this.postulantes = postulantes;
    }
}
