package com.convocatoria.pruebajoin.pruebajoin.Merit;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MeritService {

    @Autowired
    private MeritRepository meritRepository;


    public List<Merit> getAllMerit() {
        List<Merit> postulantes = new ArrayList<>();
        meritRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public Merit getMerit(long id) {
        return meritRepository.FindById(id)
                .orElse(null);

    }

    public long addMerit(Merit merit) {
        meritRepository.save(merit);
        return meritRepository.selectByIdmerit();
    }


    public void deleteMerit(long id) {
        meritRepository.DeleteByCi(id);
    }


//    public List<Auxiliary> listadoJoin(String ci){
//        return auxiliaryRepository.findPostulantesByPerson_Ci(ci);
//    }
//
//    public List<Auxiliary> listadoPorCarrera(int career){
//
//        return auxiliaryRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<Auxiliary> QueryCiCodCareer(String ci, int career){
//
//        return auxiliaryRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }
}
