package com.convocatoria.pruebajoin.pruebajoin.Merit;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class MeritController {

    @Autowired
    private MeritService meritService;

    @RequestMapping("/merit")
    public List<Merit> getAllMerit() {
        return meritService.getAllMerit();
    }

    @RequestMapping("/merit/{id}")
    public Merit getMerit(@PathVariable long id) {
        return meritService.getMerit(id);
    }

    @PostMapping("/merit")
    public long addMerit(@RequestBody Merit merit) {
        return meritService.addMerit(merit);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/merit/{id}")
    public void deleteMerit(@PathVariable long id) {
        meritService.deleteMerit(id);
    }

//    @RequestMapping("/auxiliary/listado/{ci}")
//    public List<Auxiliary> listadoJoin(@PathVariable("ci") String ci) {
//        return auxiliaryService.listadoJoin(ci);
//    }
//
//    @RequestMapping("/auxiliary/listar/{career}")
//    public List<Auxiliary> listadoPorCarrera(@PathVariable("career") int career) {
//        return auxiliaryService.listadoPorCarrera(career);
//    }
//
//    @RequestMapping("/auxiliary/listando/{ci}/{career}")
//    public List<Auxiliary> QueryCiCodCareer(@PathVariable("ci") String ci , @PathVariable("career") int career) {
//        return auxiliaryService.QueryCiCodCareer(ci,career);
//    }

//    @RequestMapping("/postulantes/listando/status")
//    public List<Boolean> getStatusFromPostulants() {
//        return postulantesService.getStatusFromPostulants();
//    }

}
