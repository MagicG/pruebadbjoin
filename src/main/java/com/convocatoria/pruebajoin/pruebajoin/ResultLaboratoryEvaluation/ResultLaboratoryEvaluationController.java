package com.convocatoria.pruebajoin.pruebajoin.ResultLaboratoryEvaluation;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class ResultLaboratoryEvaluationController {

    @Autowired
    private ResultLaboratoryEvaluationService resultLaboratoryEvaluationService;

    @RequestMapping("/resultlaboratoryevaluation")
    public List<ResultLaboratoryEvaluation> getAllResultLaboratoryEvaluation() {
        return resultLaboratoryEvaluationService.getAllResultLaboratoryEvaluation();
    }

    @RequestMapping("/resultlaboratoryevaluation/{id}")
    public ResultLaboratoryEvaluation getResultLaboratoryEvaluation(@PathVariable long id) {
        return resultLaboratoryEvaluationService.getResultLaboratoryEvaluation(id);
    }

    @PostMapping("/resultlaboratoryevaluation")
    public long addResultLaboratoryEvaluation(@RequestBody ResultLaboratoryEvaluation resultLaboratoryEvaluation) {
        return resultLaboratoryEvaluationService.addResultLaboratoryEvaluation(resultLaboratoryEvaluation);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/resultlaboratoryevaluation/{id}")
    public void deleteResultLaboratoryEvaluation(@PathVariable long id) {
        resultLaboratoryEvaluationService.deleteResultLaboratoryEvaluation(id);
    }

//    @RequestMapping("/knowledgeevaluation/listado/{ci}")
//    public List<ResultLaboratoryEvaluation> listByKnow(@PathVariable("ci") long id) {
//        return resultLaboratoryEvaluationService.listByKnow(id);
//    }


}
