package com.convocatoria.pruebajoin.pruebajoin.ResultLaboratoryEvaluation;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ResultLaboratoryEvaluationService {

    @Autowired
    private ResultLaboratoryEvaluationRepository resultLaboratoryEvaluationRepository;


    public List<ResultLaboratoryEvaluation> getAllResultLaboratoryEvaluation() {
        List<ResultLaboratoryEvaluation> postulantes = new ArrayList<>();
        resultLaboratoryEvaluationRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public ResultLaboratoryEvaluation getResultLaboratoryEvaluation(long id) {
        return resultLaboratoryEvaluationRepository.FindById(id)
                .orElse(null);

    }

    public long addResultLaboratoryEvaluation(ResultLaboratoryEvaluation resultLaboratoryEvaluation) {
        resultLaboratoryEvaluationRepository.save(resultLaboratoryEvaluation);
        return resultLaboratoryEvaluationRepository.selectByIdresultlaboratoryevaluation();
    }


    public void deleteResultLaboratoryEvaluation(long id) {
        resultLaboratoryEvaluationRepository.DeleteByCi(id);
    }


//    public List<ResultLaboratoryEvaluation> listByKnow(long id){
//        return resultLaboratoryEvaluationRepository.findKnowledgeEvaluationsByKnowledge_Idknowledge(id);
//    }
//
//    public List<Auxiliary> listadoPorCarrera(int career){
//
//        return auxiliaryRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<Auxiliary> QueryCiCodCareer(String ci, int career){
//
//        return auxiliaryRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }
}
