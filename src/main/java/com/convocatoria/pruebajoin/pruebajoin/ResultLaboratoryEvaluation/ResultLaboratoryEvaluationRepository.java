package com.convocatoria.pruebajoin.pruebajoin.ResultLaboratoryEvaluation;

//import com.tis.convocatorias.postulant.service.Career.Career;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface ResultLaboratoryEvaluationRepository extends JpaRepository<ResultLaboratoryEvaluation, Long>{

    @Query("select item from ResultLaboratoryEvaluation item where item.idresultlaboratoryevaluation = :id_Postulant")
    Optional<ResultLaboratoryEvaluation> FindById(@Param("id_Postulant") long id_Postulant);

//    @Modifying
//    @Transactional
//    @Query("update Auxiliary item set item.Status = :Status, item.person = :person, item.career = :career where item.id_Postulant = :id_Postulant")
//    void UpdateByCi(@Param("id_Postulant") long id_Postulant, @Param("Status") boolean Status, @Param("person") long person, @Param("career") int career);

    @Modifying
    @Transactional
    @Query("delete from ResultLaboratoryEvaluation item where item.idresultlaboratoryevaluation = :id_Postulant")
    void DeleteByCi(@Param("id_Postulant") long id_Postulant);


    @Query(value = "select MAX(idresultlaboratoryevaluation) from result_Laboratory_evaluation", nativeQuery = true)
    long selectByIdresultlaboratoryevaluation();



//    List<ResultLaboratoryEvaluation> findKnowledgeEvaluationsByKnowledge_Idknowledge(long idknow);

}
