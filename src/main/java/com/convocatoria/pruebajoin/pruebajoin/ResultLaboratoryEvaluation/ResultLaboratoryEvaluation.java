package com.convocatoria.pruebajoin.pruebajoin.ResultLaboratoryEvaluation;

import com.convocatoria.pruebajoin.pruebajoin.Knowledge.Knowledge;
import com.convocatoria.pruebajoin.pruebajoin.LaboratoryEvaluation.LaboratoryEvaluation;
import com.convocatoria.pruebajoin.pruebajoin.Postulantes.Postulantes;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "ResultLaboratoryEvaluation")
public class ResultLaboratoryEvaluation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idresultlaboratoryevaluation")
    private long idresultlaboratoryevaluation;
    @Column
    private double score;



//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idlaboratoryevaluation", referencedColumnName = "idlaboratoryevaluation")
//    @Column
    private LaboratoryEvaluation laboratoryEvaluation;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idpostulant", referencedColumnName = "idpostulant")
//    @Column
    private Postulantes postulantes;

//
//    @ManyToOne
//    @JoinColumn(name="id_career")
//    private Career career;

    public ResultLaboratoryEvaluation() {
    }

    public ResultLaboratoryEvaluation(double score, LaboratoryEvaluation laboratoryEvaluation, Postulantes postulantes) {
        this.score = score;
        this.laboratoryEvaluation = laboratoryEvaluation;
        this.postulantes = postulantes;
    }

    public long getIdresultlaboratoryevaluation() {
        return idresultlaboratoryevaluation;
    }

    public void setIdresultlaboratoryevaluation(long idresultlaboratoryevaluation) {
        this.idresultlaboratoryevaluation = idresultlaboratoryevaluation;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public LaboratoryEvaluation getLaboratoryEvaluation() {
        return laboratoryEvaluation;
    }

    public void setLaboratoryEvaluation(LaboratoryEvaluation laboratoryEvaluation) {
        this.laboratoryEvaluation = laboratoryEvaluation;
    }

    public Postulantes getPostulantes() {
        return postulantes;
    }

    public void setPostulantes(Postulantes postulantes) {
        this.postulantes = postulantes;
    }
}
