package com.convocatoria.pruebajoin.pruebajoin.Career;//package com.tis.convocatorias.postulant.service.Career;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CareerService {

    @Autowired
    private CareerRepository careerRepository;

    public List<Career> getAllCareer() {
        //return postulants;
        List<Career> people = new ArrayList<>();
        careerRepository.findAll()
                .forEach(people::add);
        return people;
    }

    public Career getCareer(int ci) {
        //return postulants.stream().filter(t -> t.getId().equals(id)).findFirst().get();
//        String aux = id.toString();
//        return postulantRepository.getOne(id);

        return careerRepository.FindByCi(ci)
                .orElse(null);
    }


    public void addCareer(Career career) {

        careerRepository.save(career);
    }

    public void updateCareer(int ci, Career career) {
        careerRepository.UpdateByCi(ci, career.getName());

    }

    public void deleteCareer(int ci) {
        //postulants.removeIf(p -> p.getId().equals(id));
        careerRepository.DeleteByCi(ci);
    }
}
