package com.convocatoria.pruebajoin.pruebajoin.Career;//package com.tis.convocatorias.postulant.service.Career;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class CareerController {

    @Autowired
    private CareerService careerService;

    @RequestMapping("/career")
    public List<Career> getAllCareer() {
        return careerService.getAllCareer();
    }

    @RequestMapping("/career/{id}")
    public Career getCareer(@PathVariable int id) {
        return careerService.getCareer(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/career")
    public long addCareer(@RequestBody Career career) {
        careerService.addCareer(career);
        return career.getIdcareer();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/career/{id}")
    public void updateCareer(@RequestBody Career career, @PathVariable int id) {
        careerService.updateCareer(id, career);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/career/{id}")
    public void deleteCareer(@PathVariable int id) {
        careerService.deleteCareer(id);
    }
}
