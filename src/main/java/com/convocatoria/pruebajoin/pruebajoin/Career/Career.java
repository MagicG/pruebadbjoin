package com.convocatoria.pruebajoin.pruebajoin.Career;//package com.tis.convocatorias.postulant.service.Career;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "Career")
public class Career {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
////    @Column(name = "id", updatable = false, nullable = false
//    @Transient
    @Column(name="idcareer")
    private long idcareer;
    @Column
    private int codCareer;
    @Column
    private String name;



    public Career() {
    }

    public Career(int codCareer, String name) {
        this.codCareer = codCareer;
        this.name = name;
    }

    public long getIdcareer() {
        return idcareer;
    }

    public void setIdcareer(long idcareer) {
        this.idcareer = idcareer;
    }

    public int getCodCareer() {
        return codCareer;
    }

    public void setCodCareer(int codCareer) {
        this.codCareer = codCareer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
