package com.convocatoria.pruebajoin.pruebajoin.LaboratoryEvaluation;

//import com.tis.convocatorias.postulant.service.Career.Career;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface LaboratoryEvaluationRepository extends JpaRepository<LaboratoryEvaluation, Long>{

    @Query("select item from LaboratoryEvaluation item where item.idlaboratoryevaluation = :id_Postulant")
    Optional<LaboratoryEvaluation> FindById(@Param("id_Postulant") long id_Postulant);

//    @Modifying
//    @Transactional
//    @Query("update Auxiliary item set item.Status = :Status, item.person = :person, item.career = :career where item.id_Postulant = :id_Postulant")
//    void UpdateByCi(@Param("id_Postulant") long id_Postulant, @Param("Status") boolean Status, @Param("person") long person, @Param("career") int career);

    @Modifying
    @Transactional
    @Query("delete from LaboratoryEvaluation item where item.idlaboratoryevaluation = :id_Postulant")
    void DeleteByCi(@Param("id_Postulant") long id_Postulant);


    @Query(value = "select MAX(idlaboratoryevaluation) from laboratory_evaluation", nativeQuery = true)
    long selectByIdlaboratoryevaluation();

//    List<Auxiliary> findPostulantesByPerson_Ci(String ci);
////    List<Postulantes> findPostulantesByPerson_Ci(String ci);
    List<LaboratoryEvaluation> findLaboratoryEvaluationsByAuxiliary_Idauxiliary(long idaux);

}
