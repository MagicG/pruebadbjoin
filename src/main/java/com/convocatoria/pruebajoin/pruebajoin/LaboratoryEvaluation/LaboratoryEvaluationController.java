package com.convocatoria.pruebajoin.pruebajoin.LaboratoryEvaluation;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class LaboratoryEvaluationController {

    @Autowired
    private LaboratoryEvaluationService laboratoryEvaluationService;

    @RequestMapping("/laboratoryevaluation")
    public List<LaboratoryEvaluation> getAllLaboratoryEvaluation() {
        return laboratoryEvaluationService.getAllLaboratoryEvaluation();
    }

    @RequestMapping("/laboratoryevaluation/{id}")
    public LaboratoryEvaluation getLaboratoryEvaluation(@PathVariable long id) {
        return laboratoryEvaluationService.getLaboratoryEvaluation(id);
    }

    @PostMapping("/laboratoryevaluation")
    public long addLaboratoryEvaluation(@RequestBody LaboratoryEvaluation laboratoryEvaluation) {
        laboratoryEvaluationService.addLaboratoryEvaluation(laboratoryEvaluation);
        return laboratoryEvaluation.getIdlaboratoryevaluation();
    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/laboratoryevaluation/{id}")
    public void deleteLaboratoryEvaluation(@PathVariable long id) {
        laboratoryEvaluationService.deleteLaboratoryEvaluation(id);
    }


    @RequestMapping("/laboratoryevaluation/obtain/{ci}")
    public List<LaboratoryEvaluation> listByIdAux(@PathVariable("ci") long id) {
        return laboratoryEvaluationService.listByIdAux(id);
    }


}
