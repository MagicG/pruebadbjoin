package com.convocatoria.pruebajoin.pruebajoin.LaboratoryEvaluation;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LaboratoryEvaluationService {

    @Autowired
    private LaboratoryEvaluationRepository laboratoryEvaluationRepository;


    public List<LaboratoryEvaluation> getAllLaboratoryEvaluation() {
        List<LaboratoryEvaluation> postulantes = new ArrayList<>();
        laboratoryEvaluationRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public LaboratoryEvaluation getLaboratoryEvaluation(long id) {
        return laboratoryEvaluationRepository.FindById(id)
                .orElse(null);

    }

    public long addLaboratoryEvaluation(LaboratoryEvaluation laboratoryEvaluation) {
        laboratoryEvaluationRepository.save(laboratoryEvaluation);
        return laboratoryEvaluationRepository.selectByIdlaboratoryevaluation();
    }


    public void deleteLaboratoryEvaluation(long id) {
        laboratoryEvaluationRepository.DeleteByCi(id);
    }


    public List<LaboratoryEvaluation> listByIdAux(long id){
        return laboratoryEvaluationRepository.findLaboratoryEvaluationsByAuxiliary_Idauxiliary(id);
    }
//
//    public List<Auxiliary> listadoPorCarrera(int career){
//
//        return auxiliaryRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<Auxiliary> QueryCiCodCareer(String ci, int career){
//
//        return auxiliaryRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }
}
