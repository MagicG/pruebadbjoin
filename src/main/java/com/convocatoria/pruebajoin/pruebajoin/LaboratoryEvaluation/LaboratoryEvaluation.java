package com.convocatoria.pruebajoin.pruebajoin.LaboratoryEvaluation;

import com.convocatoria.pruebajoin.pruebajoin.AcademicUnit.AcademicUnit;
import com.convocatoria.pruebajoin.pruebajoin.Area.Area;
import com.convocatoria.pruebajoin.pruebajoin.Auxiliary.Auxiliary;
import com.convocatoria.pruebajoin.pruebajoin.Knowledge.Knowledge;
import com.convocatoria.pruebajoin.pruebajoin.Thematic.Thematic;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "LaboratoryEvaluation")
public class LaboratoryEvaluation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idlaboratoryevaluation")
    private long idlaboratoryevaluation;
    @Column
    private int percentage;


//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idauxiliary", referencedColumnName = "idauxiliary")
//    @Column
    private Auxiliary auxiliary;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idthematic", referencedColumnName = "idthematic")
//    @Column
    private Thematic thematic;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idknowledge", referencedColumnName = "idknowledge")
//    @Column
    private Knowledge knowledge;

    public LaboratoryEvaluation() {
    }

    public LaboratoryEvaluation(int percentage, Auxiliary auxiliary, Thematic thematic, Knowledge knowledge) {
        this.percentage = percentage;
        this.auxiliary = auxiliary;
        this.thematic = thematic;
        this.knowledge = knowledge;
    }

    public long getIdlaboratoryevaluation() {
        return idlaboratoryevaluation;
    }

    public void setIdlaboratoryevaluation(long idlaboratoryevaluation) {
        this.idlaboratoryevaluation = idlaboratoryevaluation;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public Auxiliary getAuxiliary() {
        return auxiliary;
    }

    public void setAuxiliary(Auxiliary auxiliary) {
        this.auxiliary = auxiliary;
    }

    public Thematic getThematic() {
        return thematic;
    }

    public void setThematic(Thematic thematic) {
        this.thematic = thematic;
    }

    public Knowledge getKnowledge() {
        return knowledge;
    }

    public void setKnowledge(Knowledge knowledge) {
        this.knowledge = knowledge;
    }
}
