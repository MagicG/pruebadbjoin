package com.convocatoria.pruebajoin.pruebajoin.ResultTeachingEvaluation;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ResultTeachingEvaluationService {

    @Autowired
    private ResultTeachingEvaluationRepository resultTeachingEvaluationRepository;


    public List<ResultTeachingEvaluation> getAllResultTeachingEvaluation() {
        List<ResultTeachingEvaluation> postulantes = new ArrayList<>();
        resultTeachingEvaluationRepository.findAll()
                .forEach(postulantes::add);
        return postulantes;
    }


    public ResultTeachingEvaluation getResultTeachingEvaluation(long id) {
        return resultTeachingEvaluationRepository.FindById(id)
                .orElse(null);

    }

    public long addResultTeachingEvaluation(ResultTeachingEvaluation resultTeachingEvaluation) {
        resultTeachingEvaluationRepository.save(resultTeachingEvaluation);
        return resultTeachingEvaluationRepository.selectByIdresultteachingevaluation();
    }


    public void deleteResultTeachingEvaluation(long id) {
        resultTeachingEvaluationRepository.DeleteByCi(id);
    }

//
//    public List<ResultTeachingEvaluation> listByKnow(long id){
//        return resultTeachingEvaluationRepository.findKnowledgeEvaluationsByKnowledge_Idknowledge(id);
//    }
//
//    public List<Auxiliary> listadoPorCarrera(int career){
//
//        return auxiliaryRepository.findByCareer_CodCareerOrderByPerson(career);
//    }
//    public List<Auxiliary> QueryCiCodCareer(String ci, int career){
//
//        return auxiliaryRepository.queryByPerson_CiAndCareer_CodCareer(ci,career);
//    }
}
