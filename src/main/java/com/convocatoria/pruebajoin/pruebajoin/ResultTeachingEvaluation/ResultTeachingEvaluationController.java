package com.convocatoria.pruebajoin.pruebajoin.ResultTeachingEvaluation;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class ResultTeachingEvaluationController {

    @Autowired
    private ResultTeachingEvaluationService resultTeachingEvaluationService;

    @RequestMapping("/resultteachingevaluation")
    public List<ResultTeachingEvaluation> getAllResultTeachingEvaluation() {
        return resultTeachingEvaluationService.getAllResultTeachingEvaluation();
    }

    @RequestMapping("/resultteachingevaluation/{id}")
    public ResultTeachingEvaluation getResultTeachingEvaluation(@PathVariable long id) {
        return resultTeachingEvaluationService.getResultTeachingEvaluation(id);
    }

    @PostMapping("/resultteachingevaluation")
    public long addResultTeachingEvaluation(@RequestBody ResultTeachingEvaluation resultTeachingEvaluation) {
        return resultTeachingEvaluationService.addResultTeachingEvaluation(resultTeachingEvaluation);

    }

//    @PutMapping("/postulantes/{id}")
//    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable long id) {
//        postulantesService.updatePostulantes(id, postulantes);
//    }

    @DeleteMapping("/resultteachingevaluation/{id}")
    public void deleteResultTeachingEvaluation(@PathVariable long id) {
        resultTeachingEvaluationService.deleteResultTeachingEvaluation(id);
    }

//    @RequestMapping("/knowledgeevaluation/listado/{ci}")
//    public List<ResultTeachingEvaluation> listByKnow(@PathVariable("ci") long id) {
//        return resultTeachingEvaluationService.listByKnow(id);
//    }
//
//    @RequestMapping("/auxiliary/listar/{career}")
//    public List<Auxiliary> listadoPorCarrera(@PathVariable("career") int career) {
//        return auxiliaryService.listadoPorCarrera(career);
//    }
//
//    @RequestMapping("/auxiliary/listando/{ci}/{career}")
//    public List<Auxiliary> QueryCiCodCareer(@PathVariable("ci") String ci , @PathVariable("career") int career) {
//        return auxiliaryService.QueryCiCodCareer(ci,career);
//    }

//    @RequestMapping("/postulantes/listando/status")
//    public List<Boolean> getStatusFromPostulants() {
//        return postulantesService.getStatusFromPostulants();
//    }

}
