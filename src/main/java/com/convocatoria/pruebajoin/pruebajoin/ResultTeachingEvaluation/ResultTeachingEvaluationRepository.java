package com.convocatoria.pruebajoin.pruebajoin.ResultTeachingEvaluation;

//import com.tis.convocatorias.postulant.service.Career.Career;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface ResultTeachingEvaluationRepository extends JpaRepository<ResultTeachingEvaluation, Long>{

    @Query("select item from ResultTeachingEvaluation item where item.idresultteachingevaluation = :id_Postulant")
    Optional<ResultTeachingEvaluation> FindById(@Param("id_Postulant") long id_Postulant);

//    @Modifying
//    @Transactional
//    @Query("update Auxiliary item set item.Status = :Status, item.person = :person, item.career = :career where item.id_Postulant = :id_Postulant")
//    void UpdateByCi(@Param("id_Postulant") long id_Postulant, @Param("Status") boolean Status, @Param("person") long person, @Param("career") int career);

    @Modifying
    @Transactional
    @Query("delete from ResultTeachingEvaluation item where item.idresultteachingevaluation = :id_Postulant")
    void DeleteByCi(@Param("id_Postulant") long id_Postulant);



    @Query(value = "select MAX(idresultteachingevaluation) from result_teaching_evaluation", nativeQuery = true)
    long selectByIdresultteachingevaluation();

//    List<ResultTeachingEvaluation> findKnowledgeEvaluationsByKnowledge_Idknowledge(long idknow);

}
