package com.convocatoria.pruebajoin.pruebajoin.ResultTeachingEvaluation;

import com.convocatoria.pruebajoin.pruebajoin.Knowledge.Knowledge;
import com.convocatoria.pruebajoin.pruebajoin.KnowledgeEvaluation.KnowledgeEvaluation;
import com.convocatoria.pruebajoin.pruebajoin.Postulantes.Postulantes;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "ResultTeachingEvaluation")
public class ResultTeachingEvaluation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name="idresultteachingevaluation")
    private long idresultteachingevaluation;
    @Column
    private double score;


//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idknowledgeevaluation", referencedColumnName = "idknowledgeevaluation")
//    @Column
    private KnowledgeEvaluation knowledgeEvaluation;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idpostulant", referencedColumnName = "idpostulant")
//    @Column
    private Postulantes postulantes;

//
//    @ManyToOne
//    @JoinColumn(name="id_career")
//    private Career career;

    public ResultTeachingEvaluation() {
    }

    public ResultTeachingEvaluation(double score, KnowledgeEvaluation knowledgeEvaluation, Postulantes postulantes) {
        this.score = score;
        this.knowledgeEvaluation = knowledgeEvaluation;
        this.postulantes = postulantes;
    }

    public long getIdresultteachingevaluation() {
        return idresultteachingevaluation;
    }

    public void setIdresultteachingevaluation(long idresultteachingevaluation) {
        this.idresultteachingevaluation = idresultteachingevaluation;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public KnowledgeEvaluation getKnowledgeEvaluation() {
        return knowledgeEvaluation;
    }

    public void setKnowledgeEvaluation(KnowledgeEvaluation knowledgeEvaluation) {
        this.knowledgeEvaluation = knowledgeEvaluation;
    }

    public Postulantes getPostulantes() {
        return postulantes;
    }

    public void setPostulantes(Postulantes postulantes) {
        this.postulantes = postulantes;
    }
}
