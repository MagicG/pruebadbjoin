package com.convocatoria.pruebajoin.pruebajoin.AcademicUnit;

import com.convocatoria.pruebajoin.pruebajoin.Auxiliary.Auxiliary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface AcademicUnitRepository extends JpaRepository<AcademicUnit, Long> {

    @Query("select item from AcademicUnit item where item.idacademicunit = :codCareer")
    Optional<AcademicUnit> FindByCi(@Param("codCareer") int codCareer);

//    @Modifying
//    @Transactional
//    @Query("update AcademicUnit item set item.name = :name where item.id_aca_unit = :codCareer")
//    void UpdateByCi(@Param("codCareer") int codCareer, @Param("name") String name);

    @Modifying
    @Transactional
    @Query("delete from AcademicUnit item where item.idacademicunit = :codCareer")
    void DeleteByCi(@Param("codCareer") int id_career);



}
