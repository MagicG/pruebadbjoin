package com.convocatoria.pruebajoin.pruebajoin.AcademicUnit;//package com.tis.convocatorias.postulant.service.Career;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class AcademicUnitController {

    @Autowired
    private AcademicUnitService academicUnitService;

    @RequestMapping("/academicunit")
    public List<AcademicUnit> getAllAcademicUnits() {
        return academicUnitService.getAllAcademicUnits();
    }

    @RequestMapping("/academicunit/{id}")
    public AcademicUnit getAcademicUnit(@PathVariable int id) {
        return academicUnitService.getAcademicUnit(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/academicunit")
    public long addAcademicUnit(@RequestBody AcademicUnit academicUnit) {
        academicUnitService.addAcademicUnit(academicUnit);
        return academicUnit.getIdacademicunit();
    }

//    @RequestMapping(method = RequestMethod.PUT, value = "/academicunit/{id}")
//    public void updateAcademicUnit(@RequestBody AcademicUnit academicUnit, @PathVariable int id) {
//        academicUnitService.updateAcademicUnit(id, academicUnit);
//    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/acadeemicunit/{id}")
    public void deleteAcademicUnit(@PathVariable int id) {
        academicUnitService.deleteAcademicUnit(id);
    }
}
