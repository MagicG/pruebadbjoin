package com.convocatoria.pruebajoin.pruebajoin.AcademicUnit;//package com.tis.convocatorias.postulant.service.Career;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AcademicUnitService {

    @Autowired
    private AcademicUnitRepository academicUnitRepository;

    public List<AcademicUnit> getAllAcademicUnits() {
        List<AcademicUnit> people = new ArrayList<>();
        academicUnitRepository.findAll()
                .forEach(people::add);
        return people;
    }

    public AcademicUnit getAcademicUnit(int ci) {
        return academicUnitRepository.FindByCi(ci)
                .orElse(null);
    }


    public void addAcademicUnit(AcademicUnit academicUnit) {

        academicUnitRepository.save(academicUnit);
    }

//    public void updateAcademicUnit(int ci, AcademicUnit academicUnit) {
//        academicUnitRepository.UpdateByCi(ci, academicUnit.getName());
//
//    }

    public void deleteAcademicUnit(int ci) {
        academicUnitRepository.DeleteByCi(ci);
    }
}
