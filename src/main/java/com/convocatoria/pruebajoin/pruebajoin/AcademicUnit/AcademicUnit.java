package com.convocatoria.pruebajoin.pruebajoin.AcademicUnit;//package com.tis.convocatorias.postulant.service.Career;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "AcademicUnit")
public class AcademicUnit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
////    @Column(name = "id", updatable = false, nullable = false
//    @Transient
    @Column(name="idacademicunit")
    private long idacademicunit;

    @Column
    private String name;



    public AcademicUnit() {
    }

    public AcademicUnit( String name) {
        this.name = name;
    }

    public long getIdacademicunit() {
        return idacademicunit;
    }

    public void setIdacademicunit(long idacademicunit) {
        this.idacademicunit = idacademicunit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
